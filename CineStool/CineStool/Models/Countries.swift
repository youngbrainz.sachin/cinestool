//
//  Countries.swift
//  SMA
//
//  Created by Jignesh Kugashiya on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import Foundation

struct Countries: Decodable
{
    let code: String?
    let name: String?
    let country: String?
}
