//
//  DashboardModel.swift
//  CineStool
//
//  Created by YB on 16/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import Foundation
import ObjectMapper

class DashboardModel: Mappable {
    var is_app_live: String = ""
    var banner_movies: [BannerHome] = []
    var categories: [DashboardCategories] = []
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        banner_movies      <- map["banner_movies"]
        categories         <- map["categories"]
        is_app_live        <- map["is_app_live"]
    }
}

class DashboardCategories: Mappable {
    var name: String = ""
    var data: [Movies] = []
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        name          <- map["name"]
        data          <- map["data"]
    }
}


class BannerHome: Mappable {
    var _id: String = ""
    var banner_id: String = ""
    var banner_image: String = ""
    var banner_video: String = ""
    var created_at: String = ""
    var created_date: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var status: String = ""
    var updated_at: String = ""
    var year: String = ""
    
    init?() {
    
    }
    
    required init?(map: Map) {
    
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                      <- map["_id"]
        banner_id                      <- map["banner_id"]
        banner_image                      <- map["banner_image"]
        banner_video                      <- map["banner_video"]
        created_at                      <- map["created_at"]
        created_date                      <- map["created_date"]
        modified_date                      <- map["modified_date"]
        month                      <- map["month"]
        month_name                      <- map["month_name"]
        status                      <- map["status"]
        updated_at                      <- map["updated_at"]
        year                      <- map["year"]
    }
}
