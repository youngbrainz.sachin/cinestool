//
//  Advertisement.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 26/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import Foundation
import ObjectMapper

class Advertisement: Mappable {
    var _id: String = ""
    var advertisement_image: String = ""
    var advertisement_price: String = ""
    var advertisement_type: String = ""
    var advertisement_url: String = ""
    var created_date: String = ""
    var extn: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var movie_id: String = ""
    var name: String = ""
    var status: String = ""
    var year: String = ""
    var advertisement_actual_price: String = ""
    var currency_symbol: String = ""
    var currency_actual_symbol: String = ""

    init?() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                      <- map["_id"]
        advertisement_image      <- map["advertisement_image"]
        advertisement_price      <- map["advertisement_price"]
        advertisement_type       <- map["advertisement_type"]
        advertisement_url        <- map["advertisement_url"]
        created_date             <- map["created_date"]
        extn                     <- map["extn"]
        modified_date            <- map["modified_date"]
        month                    <- map["month"]
        month_name               <- map["month_name"]
        movie_id                 <- map["movie_id"]
        name                     <- map["name"]
        status                   <- map["status"]
        year                     <- map["year"]
        advertisement_actual_price <- map["advertisement_actual_price"]
        currency_symbol <- map["currency_symbol"]
        currency_actual_symbol <- map["currency_actual_symbol"]
    }
}


