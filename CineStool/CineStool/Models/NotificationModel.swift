//
//  NotificationModel.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 29/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationModel: Mappable {
    var _id: String = ""
    var created_at: String = ""
    var date: String = ""
    var from_id: String = ""
    var is_read: String = ""
    var message: String = ""
    var movie_id: String = ""
    var send_user_type: String = ""
    var to_id: String = ""
    var to_user_type: String = ""
    var type: String = ""
    var updated_at: String = ""
    
    init?() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                         <- map["_id"]
        created_at                  <- map["created_at"]
        date                        <- map["date"]
        from_id                     <- map["from_id"]
        is_read                     <- map["is_read"]
        message                     <- map["message"]
        movie_id                    <- map["movie_id"]
        send_user_type              <- map["send_user_type"]
        to_id                       <- map["to_id"]
        to_user_type                <- map["to_user_type"]
        type                        <- map["type"]
        updated_at                  <- map["updated_at"]
    }
}
