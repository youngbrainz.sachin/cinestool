//
//  SubscriptionModel.swift
//  CineStool
//
//  Created by MacBook Air 002 on 21/02/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import Foundation
import ObjectMapper

class SubscriptionModel: Mappable {
    var _id: String = ""
    var created_at: String = ""
    var created_date: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var plan: String = ""
    var price: String = ""
    var updated_at: String = ""
    var year: String = ""
    var is_selected_plan: String = ""
    var actual_amount: String = ""
    var plan_number: String = ""
    
    init?() {
        
    }

    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id      <- map["_id"]
        created_at         <- map["created_at"]
        created_date        <- map["created_date"]
        modified_date         <- map["modified_date"]
        month        <- map["month"]
        month_name         <- map["month_name"]
        plan        <- map["plan"]
        price         <- map["price"]
        updated_at        <- map["updated_at"]
        year        <- map["year"]
        is_selected_plan        <- map["is_selected_plan"]
        actual_amount        <- map["actual_amount"]
        plan_number        <- map["plan_number"]
    }
}



class cardListingModel: Mappable {
    var _id: String = ""
    var card_number: String = ""
    var created_at: String = ""
    var created_date: String = ""
    var cvv: String = ""
    var expiry_date: String = ""
    var holder_name: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var updated_at: String = ""
    var user_id: String = ""
    var year: String = ""
    var is_default_card: String = ""
    
    init?() {
        
    }

    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id      <- map["_id"]
        card_number      <- map["card_number"]
        created_at         <- map["created_at"]
        created_date        <- map["created_date"]
        cvv        <- map["cvv"]
        expiry_date        <- map["expiry_date"]
        holder_name        <- map["holder_name"]
        modified_date         <- map["modified_date"]
        month        <- map["month"]
        month_name         <- map["month_name"]
        updated_at        <- map["updated_at"]
        year        <- map["year"]
        user_id        <- map["user_id"]
        is_default_card        <- map["is_default_card"]
    }
}


class paymentHistory: Mappable {
    var _id: String = ""
    var amount: String = ""
    var card_number: String = ""
    var created_at: String = ""
    var created_date: String = ""
    var customer_id: String = ""
    var email_id: String = ""
    var expired_date: String = ""
    var expired_date_strtotime: String = ""
    var is_auto_subscribed: String = ""
    var mobile_no: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var msg: String = ""
    var payment_date: String = ""
    var payment_date_time: String = ""
    var payment_time: String = ""
    var payment_type: String = ""
    var payment_type_id: String = ""
    var start_date: String = ""
    var status: String = ""
    var subscription_id: String = ""
    var totalMonth: String = ""
    var transcation_id: String = ""
    var updated_at: String = ""
    var user_name: String = ""
    var year: String = ""

    init?() {
        
    }

    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id      <- map["_id"]
        amount      <- map["amount"]
        card_number      <- map["card_number"]
        created_at      <- map["created_at"]
        created_date      <- map["created_date"]
        customer_id      <- map["customer_id"]
        email_id      <- map["email_id"]
        expired_date      <- map["expired_date"]
        expired_date_strtotime      <- map["expired_date_strtotime"]
        is_auto_subscribed      <- map["is_auto_subscribed"]
        mobile_no      <- map["mobile_no"]
        modified_date      <- map["modified_date"]
        month      <- map["month"]
        month_name      <- map["month_name"]
        msg      <- map["msg"]
        payment_date      <- map["payment_date"]
        payment_date_time      <- map["payment_date_time"]
        payment_time      <- map["payment_time"]
        payment_type      <- map["payment_type"]
        payment_type_id      <- map["payment_type_id"]
        start_date      <- map["start_date"]
        status      <- map["status"]
        subscription_id      <- map["subscription_id"]
        totalMonth      <- map["totalMonth"]
        transcation_id      <- map["transcation_id"]
        updated_at      <- map["updated_at"]
        user_name      <- map["user_name"]
        year      <- map["year"]
    }
}
