//
//  Categories.swift
//  CineStool
//
//  Created by MacBook Air 002 on 15/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import Foundation
import ObjectMapper

class Categories: Mappable {
    var _id: String = ""
    var category_image: String = ""
    var created_date: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var name: String = ""
    var status: String = ""
    var year: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var subCategories: [SubCategories] = []
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                      <- map["_id"]
        category_image           <- map["category_image"]
        created_date             <- map["created_date"]
        modified_date            <- map["modified_date"]
        month                    <- map["month"]
        month_name               <- map["month_name"]
        name                     <- map["name"]
        status                   <- map["status"]
        year                     <- map["year"]
        created_at               <- map["created_at"]
        updated_at               <- map["updated_at"]
        subCategories            <- map["get_subcategory"]
    }
}


class SubCategories: Mappable {
    var _id: String = ""
    var category_id: String = ""
    var created_date: String = ""
    var modified_date: String = ""
    var month: String = ""
    var month_name: String = ""
    var name: String = ""
    var status: String = ""
    var year: String = ""
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        _id                      <- map["_id"]
        category_id              <- map["category_id"]
        created_date             <- map["created_date"]
        modified_date            <- map["modified_date"]
        month                    <- map["month"]
        month_name               <- map["month_name"]
        name                     <- map["name"]
        status                   <- map["status"]
        year                     <- map["year"]
    }
}
