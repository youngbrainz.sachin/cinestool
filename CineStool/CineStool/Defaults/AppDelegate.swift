//
//  AppDelegate.swift
//  CineStool
//
//  Created by MacBook Air 002 on 09/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//
//#Old CineStool#

import UIKit
import CoreData
import KWDrawerController
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import Siren
import SwiftKeychainWrapper
import AVKit
import GoogleCast

enum forSubscriptionPopup {
    case none
    case isShowSubscrion
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GCKLoggerDelegate {

    var window: UIWindow?
    var navController: UINavigationController?
    var sideMenuVC : SideMenuVC!

    var loginUser_Id = String()
    var userSessionToken = String()
    var deviceToken = String()
    var loggedInUserData: User?
    var userType = "2"
    var movieCategories: [Categories] = []
    var blockPaymentDone: (()->())!
    var deviceUUID = String()
    var isFullScreen = false
    var isFirstTime = true
    var isPressedNoToSubscriptionPopup = false
    var isAppLive = ""
    var is_Subscribed = false
    var badgeCount = 0
    var container = MainContainerVC()
    var isCallAPI = true
    var imgProfilePicture = UIImage()
    var profileData = [String: Any]()
    var isProfileChange = false
    var isJustLoggedIn = false

    let kReceiverAppID = kGCKDefaultMediaReceiverApplicationID
    let kDebugLoggingEnabled = true
    var arrDashboardData: DashboardModel!
    
    var bannerMovies: [BannerHome] = []
    var trandingMovies: [Movies] = []
    var popularMovies: [Movies] = []
    var myHistory: [Movies] = []

    var isFrom:forSubscriptionPopup = .none
    
    var subscription_status:String = "3"
    var subscription_id:String = "0"
    
    var Loginpassword = ""
    
    var myOrientation: UIInterfaceOrientationMask = .portrait
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playback)
        } catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
        
        // Set your receiver application ID.
        let criteria = GCKDiscoveryCriteria(applicationID: kReceiverAppID)
        let options = GCKCastOptions(discoveryCriteria: criteria)
        options.physicalVolumeButtonsWillControlDeviceVolume = true
        GCKCastContext.setSharedInstanceWith(options)

        // Theme using GCKUIStyle.
        let castStyle = GCKUIStyle.sharedInstance()
        // Set the property of the desired cast widgets.
        castStyle.castViews.deviceControl.buttonTextColor = .darkGray
        // Refresh all currently visible views with the assigned styles.
        castStyle.apply()
//
//        // Enable default expanded controller.
        GCKCastContext.sharedInstance().useDefaultExpandedMediaControls = true
//
//        // Enable logger.
        GCKLogger.sharedInstance().delegate = self
//
//        // Set logger filter.
//        let filter = GCKLoggerFilter()
//        filter.minimumLevel = .error
//        GCKLogger.sharedInstance().filter = filter


        setupProfile()
        
        if UserDefaults.standard.bool(forKey: UserdefaultsConstants.isJustLogin) == true{
            appDel.isJustLoggedIn = true
        }else{
            appDel.isJustLoggedIn = false
        }
        
        if UserDefaults.standard.bool(forKey: UserdefaultsConstants.isPressedNoToSubscriptionPopup) == true{
            appDel.isPressedNoToSubscriptionPopup = true
        }else{
            appDel.isPressedNoToSubscriptionPopup = false
        }
        
        if UserDefaults.standard.bool(forKey: "isTouchID") == true {
            let viewController = BiometricAuthVC(nibName: "BiometricAuthVC", bundle: .main)
            navController = UINavigationController(rootViewController: viewController)
            self.navController?.isNavigationBarHidden = true
            window = UIWindow(frame: UIScreen.main.bounds)
            if #available(iOS 13.0, *) {
                window!.overrideUserInterfaceStyle = .light
            }
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
        } else {
            displayScreen()
        }
        
        setupNotification(application: application, launchoption: launchOptions ?? [:])
        Siren.shared.launchAppStore()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UIApplication.shared.applicationIconBadgeNumber = 0
        return true
    }
    
    func forceLocalizationCustomizationPresentationExample() {
        let siren = Siren.shared
        siren.rulesManager = RulesManager(majorUpdateRules: .critical,
                                          minorUpdateRules: .annoying,
                                          patchUpdateRules: .default,
                                          revisionUpdateRules: Rules(promptFrequency: .weekly, forAlertType: .option))

        siren.wail { results in
            switch results {
            case .success(let updateResults):
                print("AlertAction ", updateResults.alertAction)
                print("Localization ", updateResults.localization)
                print("Model ", updateResults.model)
                print("UpdateType ", updateResults.updateType)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        if let rootViewController = self.topViewControllerWithRootViewController(rootViewController: window?.rootViewController) {
//            if (rootViewController.responds(to: Selector("canRotate"))) {
//                // Unlock landscape view orientations for this view controller
//                return .landscape;
//            }
//        }
//        // Only allow portrait (standard behaviour)
//        return .portrait;
//    }
    
    private func topViewControllerWithRootViewController(rootViewController: UIViewController!) -> UIViewController? {
        if (rootViewController == nil) { return nil }
        if (rootViewController.isKind(of: UITabBarController.self)) {
            return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UITabBarController).selectedViewController)
        } else if (rootViewController.isKind(of: UINavigationController.self)) {
            return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UINavigationController).visibleViewController)
        } else if (rootViewController.presentedViewController != nil) {
            return topViewControllerWithRootViewController(rootViewController: rootViewController.presentedViewController)
        }
        return rootViewController
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication){
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        forceLocalizationCustomizationPresentationExample()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CineStool")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - GCKLoggerDelegate

    func logMessage(_ message: String,
                    at _: GCKLoggerLevel,
                    fromFunction function: String,
                    location: String) {
      if kDebugLoggingEnabled {
        print("\(location): \(function) - \(message)")
      }
    }
}

//MARK:- SetUp Profile
extension AppDelegate{
    func setupProfile()  {
        if let userdata = UserDefaults.standard.value(forKey: UserdefaultsConstants.userData) as? [String:Any]{
            loggedInUserData = User (JSON: userdata)
            loginUser_Id = loggedInUserData?._id ?? ""
            userSessionToken = loggedInUserData?.session_token ?? ""
        }
    }
    
    func displayScreen()  {
        window = UIWindow(frame:UIScreen.main.bounds)
        if loginUser_Id != "" {
            if loggedInUserData?.new_user == "1" {
                let viewController = CreateProfileVC(nibName: "CreateProfileVC", bundle: .main)
                viewController.isFrom = .mobileVerify
                viewController.mobileNumber = loggedInUserData?.mobile_no ?? ""
                viewController.mobileCode = loggedInUserData?.country_code ?? ""
                navController = UINavigationController(rootViewController: viewController)
            }else{
                let mainViewController = MainContainerVC(nibName: "MainContainerVC", bundle: .main)
                let menuViewController = SideMenuVC(nibName: "SideMenuVC", bundle: .main)
                //Set for callback use
                sideMenuVC = menuViewController
                
                let aDrawer = DrawerController()
                aDrawer.setViewController(mainViewController, for: .none)
                aDrawer.setViewController(menuViewController, for: .left)
                
                aDrawer.drawerWidth = Float((UIScreen.main.bounds.size.width / 3) * 2) + 30
//                let castContainerVC =
//                        GCKCastContext.sharedInstance().createCastContainerController(for: aDrawer)
//                castContainerVC.miniMediaControlsItemEnabled = true

                navController = UINavigationController(rootViewController: aDrawer)
            }
        } else {
            self.isFrom = .isShowSubscrion
            let viewController = MobileVerifyVC(nibName: "MobileVerifyVC", bundle: .main)
            navController = UINavigationController(rootViewController: viewController)
        }
        self.navController?.isNavigationBarHidden = true
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
    
    func getDeviceUUID() -> String {
        if KeychainWrapper.standard.string(forKey: "deviceUUID") != nil{
            deviceUUID = KeychainWrapper.standard.string(forKey: "deviceUUID")!
        }else{
            deviceUUID = UUID.init().uuidString
            deviceUUID = deviceUUID.replacingOccurrences(of: "-", with: "")
            KeychainWrapper.standard.set(deviceUUID, forKey: "deviceUUID")
        }
        return deviceUUID
    }
}

//MARK:- Redirect User to respective Screen
extension AppDelegate {
    func displayScreen(VC: UIViewController.Type, nib: String, sideMenuConfig: Bool) {
        if sideMenuConfig {
            sideMenuConfiguartion(VC: VC, nib: nib)
            return
        }
        window = UIWindow(frame:UIScreen.main.bounds)
        let viewController = VC.init(nibName: nib, bundle: .main)
        navController = UINavigationController(rootViewController: viewController)
        self.navController?.isNavigationBarHidden = true
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
    
    //MARK :- SideMenu Configuration
    func sideMenuConfiguartion(VC: UIViewController.Type, nib: String) {
        let mainViewController = VC.init(nibName: nib, bundle: .main)
        let menuViewController = SideMenuVC(nibName: "SideMenuVC", bundle: .main)
        //Set for callback use
        sideMenuVC = menuViewController
        
        let aDrawer = DrawerController()
        aDrawer.setViewController(mainViewController, for: .none)
        aDrawer.setViewController(menuViewController, for: .left)
        
        aDrawer.drawerWidth = Float((UIScreen.main.bounds.size.width / 3) * 2) + 30
//        let castContainerVC =
//                GCKCastContext.sharedInstance().createCastContainerController(for: aDrawer)
//        castContainerVC.miniMediaControlsItemEnabled = true
        navController = UINavigationController(rootViewController: aDrawer)
        self.navController?.isNavigationBarHidden = true
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
}

//MARK:- Notification Setup and Delegates
extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    func setupNotification(application: UIApplication, launchoption: [UIApplication.LaunchOptionsKey: Any])  {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions,completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        let remoteNotification: NSDictionary! = launchoption[.remoteNotification] as? NSDictionary
        if (remoteNotification != nil)
        {
            let userInfo = remoteNotification as? [String:Any]

            if let tags = userInfo?["tag"] as? String{
                redirectFromNotification(tag: tags, data: userInfo ?? [:])
            }
        }
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        Messaging.messaging().delegate = self
        FirebaseApp.configure()
        application.registerForRemoteNotifications()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        self.badgeCount += 1
        NotificationCenter.default.post(name: .badge, object: nil, userInfo: ["badge": self.badgeCount])
        
        let userInfo = notification.request.content.userInfo
        print("User Info",userInfo)
        center.removeAllDeliveredNotifications()
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        let userInfo = response.notification.request.content.userInfo
        if let tags = userInfo["tag"] as? String{
            redirectFromNotification(tag: tags, data: userInfo as! [String : Any])
            self.badgeCount -= 1
            NotificationCenter.default.post(name: .badge, object: nil, userInfo: ["badge": self.badgeCount])
        }
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print("Remotemessage",remoteMessage.appData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("deviceTokenString",deviceTokenString)
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            }
            else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.setValue("\(result.token)", forKey: "token")
                UserDefaults.standard.synchronize()
                self.deviceToken = result.token
            }
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)  {
        print("Firebase registration token: \(fcmToken)")
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])  {
        print("my push is: %@", userInfo)
        guard application.applicationState == UIApplication.State.inactive else { return }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("my push is: %@", userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func redirectFromNotification(tag: String, data: [String:Any]){
        if tag == "5" {
            let objTab = MovieDetailVC(nibName: "MovieDetailVC", bundle: .main)
            objTab.movieID = data["movie_id"] as? String ?? ""
//            let castContainerVC =
//                    GCKCastContext.sharedInstance().createCastContainerController(for: objTab)
//            castContainerVC.miniMediaControlsItemEnabled = true
            navController?.pushViewController(objTab, animated: true)
        }
        else if tag == "2" {
        }
    }
    
    func redirectToMovieDetail(movieID: String) {
        let objTab = MovieDetailVC(nibName: "MovieDetailVC", bundle: .main)
        objTab.movieID = movieID
        let castContainerVC =
                GCKCastContext.sharedInstance().createCastContainerController(for: objTab)
        castContainerVC.miniMediaControlsItemEnabled = true
        findtopViewController()?.navigationController?.pushViewController(castContainerVC, animated: true)
    }
}
