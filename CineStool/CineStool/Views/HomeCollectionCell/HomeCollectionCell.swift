//
//  HomeCollectionCell.swift
//  CineStool
//
//  Created by YB on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell, ReusableView, NibLoadableView {
    
    @IBOutlet weak var imgPoster: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
