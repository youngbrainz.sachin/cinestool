//
//  NotificationTblCell.swift
//  CineStool
//
//  Created by YB on 24/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class NotificationTblCell: UITableViewCell {

    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
 
    
}
