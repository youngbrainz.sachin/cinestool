//
//  MyCollectionTblCell.swift
//  CineStool
//
//  Created by YB on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class MyCollectionTblCell: UITableViewCell,ReusableView, NibLoadableView {
    
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblRemainingTime: UILabel!
    @IBOutlet weak var imgRemainingTimer: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
