//
//  AVPlayerExtension.swift
//  CineStool
//
//  Created by MacBook Air 002 on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import AVFoundation



extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

extension AVPlayerItem {
    enum TrackType {
        case subtitle
        case audio                        /**             Return valid AVMediaSelectionGroup is item is available.             */
        fileprivate func characteristic(item:AVPlayerItem) -> AVMediaSelectionGroup?  {
            let str = self == .subtitle ? AVMediaCharacteristic.legible : AVMediaCharacteristic.audible
            if item.asset.availableMediaCharacteristicsWithMediaSelectionOptions.contains(str)
            {
                return item.asset.mediaSelectionGroup(forMediaCharacteristic: str)
                
            }
            return nil
        }
    }
    func tracks(type:TrackType) -> [String] {
        if let characteristic = type.characteristic(item: self)
        {
            return characteristic.options.map { $0.displayName }
            
        }
        return [String]()
        
    }
    func selected(type:TrackType) -> String? {
        guard let group = type.characteristic(item: self) else {                return nil
            
        }
        let selected = self.selectedMediaOption(in: group)
        return selected?.displayName
        
    }
    func select(type:TrackType, name:String) -> Bool {            guard let group = type.characteristic(item: self) else {                return false
        
        }
        guard let matched = group.options.filter({ $0.displayName == name }).first else {
            return false
            
        }
        self.select(matched, in: group)
        return true
        
    }
    
}
