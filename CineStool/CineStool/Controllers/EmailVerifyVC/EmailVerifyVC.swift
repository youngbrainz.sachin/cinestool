//
//  EmailVerifyVC.swift
//  CineStool
//
//  Created by MacBook Air 002 on 02/01/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

enum isFrom {
    case none
    case fromForgotPassword
    case fromForgotMobile
}

class EmailVerifyVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    
    let InsertEmail = "Please insert Email."
    let InsertValidEmail = "Please insert Valid Email."
    var isfrom:isFrom = .none
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        
        if isfrom == .fromForgotMobile{
            self.lblHeader.text = "Enter valid email address to change phone number"
            self.lblSubHeader.text = "" //"Forgot Phone Number"
        }else{
            self.lblHeader.text = "Enter valid email address to reset your password"
            self.lblSubHeader.text = "" //"Forgot Password"
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnVerifyTapped(_ sender: UIButton) {
        if validate(){
            if self.isfrom == .fromForgotMobile{
                self.forgotMobileAPI(email: self.txtEmail.text ?? "")
            }else{
                self.forgotPasswordAPI(email: self.txtEmail.text ?? "")
            }
        }
    }
}

extension EmailVerifyVC{
    //Validate Fields
    func validate() -> Bool {
        if txtEmail.text?.count == 0 {
            alertOk(title: "", message: InsertEmail)
            return false
        }else if !(Validate.isValidEmail(testStr: self.txtEmail.text!)) {
            alertOk(title: "", message: InsertValidEmail)
            return false
        }
        return true
    }

    func forgotMobileAPI(email:String)
    {
        let params = ["email_id":email] as [String : Any]
        WebService.Request.patch(url: forgotMobileNo, type: .post, parameter: params, callSilently: false, header: nil) { (response, error) in
            if error == nil {
                print(response!)
                if response!["status"] as? Bool == true
                {
                        let objTab = OTPVerifyVC(nibName: "OTPVerifyVC", bundle: nil)
                        objTab.isFrom = .emailScreen
                        objTab.mobileCode = response!["country_code"] as? String ?? ""
                        objTab.mobileNumber = response!["phone_number"] as? String ?? ""
                        objTab.otp = response!["otp"] as? String ?? ""
                        self.view.endEditing(true)
                        self.txtEmail.text = ""
                    self.navigationController!.pushViewController(objTab, animated: true)
                } else{
                    self.alertOk(title: "", message: response!["msg"] as? String ?? "Something Went Wrong..")
                }
            }else{
                self.alertOk(title: "", message:"Something Went Wrong..")
            }
        }
    }
    
    func forgotPasswordAPI(email:String)
    {
        let params = ["email_id":email] as [String : Any]
        WebService.Request.patch(url: forgotPassword, type: .post, parameter: params, callSilently: false, header: nil) { (response, error) in
            if error == nil {
                print(response!)
                if response!["status"] as? Bool == true
                {
                        let objTab = OTPVerifyVC(nibName: "OTPVerifyVC", bundle: nil)
                        objTab.isFrom = .changePasswordScreen
                        objTab.otp = response!["otp"] as? String ?? ""
                        objTab.regEmail = self.txtEmail.text ?? ""
                        self.view.endEditing(true)
                        self.txtEmail.text = ""
                    self.navigationController!.pushViewController(objTab, animated: true)
                } else{
                    self.alertOk(title: "", message: response!["msg"] as? String ?? "Something Went Wrong..")
                }
            }else{
                self.alertOk(title: "", message:"Something Went Wrong..")
            }
        }
    }
    
    
}
