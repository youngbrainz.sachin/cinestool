//
//  MainContainerVC.swift
//  CineStool
//
//  Created by YB on 23/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class ViewTab: UIView {
    @IBOutlet weak var btnTabHome: UIButton!
    @IBOutlet weak var btnTabCollection: UIButton!
    @IBOutlet weak var btnTabNotification: UIButton!
    @IBOutlet weak var btnTabProfile: UIButton!
    
    var blockTab: (()->())!
    
    @IBAction func btnTabAction(_ sender: UIButton) {
        blockTab()
    }
}

class MainContainerVC: UIViewController {

    @IBOutlet var mainContainer: UIView!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var viewTabHome: ViewTab!
    @IBOutlet weak var viewTabCollection: ViewTab!
    @IBOutlet weak var viewTabNotification: ViewTab!
    @IBOutlet weak var viewTabProfile: ViewTab!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var heightHeaderView: NSLayoutConstraint!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var lineLeading: NSLayoutConstraint!
    @IBOutlet weak var lblBadge: UILabel!
    
    var isfor = "Home"
    var notificationVC: NotificationVC!
    var mycollectionVC: MyCollectionVC!
    var createprofileVC: CreateProfileVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startAnimatingLoader(message: "")
        onloadOperation()
        NotificationCenter.default.addObserver(self, selector: #selector(getBadge), name: .badge, object: nil)
    }
    
    @objc func getBadge(_ notification: Notification) {
        if let data = notification.userInfo as? [String: Any] {
            let count = data["badge"] as! Int
            lblBadge.text = "\(count)"
            
            if self.isfor != "Notifications" {
                if appDel.badgeCount != 0 {
                    lblBadge.isHidden = false
                } else {
                    lblBadge.isHidden = true
                }
            } else {
                lblBadge.isHidden = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if ((findtopViewController() as? LandscapeAVPlayerController) != nil) {
            return
        }
        if isfor == "Home" {
            self.homeSetup(setContainer: true)
        }
        else if isfor == "My Collection" {
            self.myColletionSetup(headerText: "My Collection", listTyoe: "1")
        } else if isfor == "History" {
            self.myColletionSetup(headerText: "History", listTyoe: "2")
        }
        else if isfor == "My Profile" {
            self.btnSearch.isHidden = true
            self.imgHeader.isHidden = true
            self.lblHeader.isHidden = true
            self.viewHeader.isHidden = false
            self.heightHeaderView.constant = 50
            self.superView.backgroundColor = #colorLiteral(red: 0.007843137255, green: 0.1058823529, blue: 0.2039215686, alpha: 1)
            self.viewHeader.backgroundColor = #colorLiteral(red: 0.007843137255, green: 0.1058823529, blue: 0.2039215686, alpha: 1)
            self.lineWidth(width: 3, line: self.lineLeading, view: self.viewTabHome)
            setContiner(VC: "CreateProfileVC", nibClass: CreateProfileVC.self, parent: MainContainerVC.self, container: self.mainContainer, newController: { (controller) in
                if let new = controller as? CreateProfileVC {
                    self.createprofileVC = new
                    new.blockDidDismiss = {
                        self.btnSideMenu.setImage(#imageLiteral(resourceName: "FourSquare"), for: .normal)
                    }
                }
            })
        }
    }
    //"Do you want to subscribe to our subscription plans to watch all movies free? "
    override func viewDidAppear(_ animated: Bool) {
//        if appDel.isFrom == .isShowSubscrion{
//            appDel.isFrom = .none
//            alertTwoButton(title: "Subscription", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Do you want to enroll in cinestool monthly subscription?") { (result) in
//                if result{
//                    let objPrivacyPolicyVC = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
//                    self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
//                }
//            }
//        }
    }
    
    func onloadOperation() {
        if appDel.isFirstTime {
            self.homeSetup(setContainer: true)
            appDel.isFirstTime = false
        }
        
        viewTabHome.blockTab = {
            self.homeSetup(setContainer: true)
        }
        
        viewTabCollection.blockTab = {
            self.myColletionSetup(headerText: "My Collection", listTyoe: "1")
        }
        
        viewTabNotification.blockTab = {
            self.lblBadge.isHidden = true
            self.lblBadge.text = "0"
            self.isfor = "Notifications"
            self.btnSearch.isHidden = true
            self.imgHeader.isHidden = true
            self.lblHeader.isHidden = false
            self.lblHeader.text = "Notifications"
            self.viewHeader.isHidden = false
            self.heightHeaderView.constant = 50
            self.superView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.viewHeader.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.lineWidth(width: 2, line: self.lineLeading, view: self.viewLine)
            setContiner(VC: "NotificationVC", nibClass: NotificationVC.self, parent: MainContainerVC.self, container: self.mainContainer, newController: { (controller) in
                if let new = controller as? NotificationVC {
                    self.notificationVC = new
                }
            })
        }
        
        viewTabProfile.blockTab = {
            self.isfor = "My Profile"
            self.btnSearch.isHidden = true
            self.imgHeader.isHidden = true
            self.lblHeader.isHidden = true
            self.viewHeader.isHidden = false
            self.heightHeaderView.constant = 50
            self.superView.backgroundColor = #colorLiteral(red: 0.007843137255, green: 0.1058823529, blue: 0.2039215686, alpha: 1)
            self.viewHeader.backgroundColor = #colorLiteral(red: 0.007843137255, green: 0.1058823529, blue: 0.2039215686, alpha: 1)
            self.lineWidth(width: 3, line: self.lineLeading, view: self.viewLine)
            setContiner(VC: "CreateProfileVC", nibClass: CreateProfileVC.self, parent: MainContainerVC.self, container: self.mainContainer, newController: { (controller) in
                if let new = controller as? CreateProfileVC {
                    self.createprofileVC = new
                    new.blockDidDismiss = {
                        self.btnSideMenu.setImage(#imageLiteral(resourceName: "FourSquare"), for: .normal)
                    }
                }
            })
        }
    }
    
    func homeSetup(setContainer: Bool) {
        lineWidth(width: 0, line: lineLeading, view: viewTabHome)
        self.isfor = "Home"
        self.viewHeader.isHidden = false
        self.btnSearch.isHidden = false
        self.imgHeader.isHidden = false
        self.lblHeader.isHidden = true
        self.superView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewHeader.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.heightHeaderView.constant = 50
        btnSearch.setImage(#imageLiteral(resourceName: "Search"), for: .normal)
        btnSearch.setTitle("", for: .normal)
        if setContainer {
            setContiner(VC: "HomeVC", nibClass: HomeVC.self, parent: MainContainerVC.self, container: self.mainContainer)
        }
    }
    
    func myColletionSetup(headerText: String, listTyoe: String) {
        self.isfor = "My Collection"
        self.btnSearch.isHidden = true
        self.imgHeader.isHidden = true
        self.lblHeader.isHidden = false
        self.lblHeader.text = headerText //"My Collection"
        self.viewHeader.isHidden = false
        self.heightHeaderView.constant = 50
        self.superView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewHeader.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.lineWidth(width: 1, line: self.lineLeading, view: self.viewLine)
        setContiner(VC: "MyCollectionVC", nibClass: MyCollectionVC.self, parent: MainContainerVC.self, container: self.mainContainer, newController: { (controller) in
            if let new = controller as? MyCollectionVC {
                self.mycollectionVC = new
                new.listType = listTyoe
            }
        })
    }
    
    func lineWidth(width: CGFloat, line: NSLayoutConstraint, view: UIView) {
        UIView.animate(withDuration: 0.3) {
            line.constant = ((self.view.bounds.width / 4) * width)
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        let objTab = MovieListingVC(nibName: "MovieListingVC", bundle: .main)
        objTab.isFrom = .home
        objTab.blockBack = {
            self.homeSetup(setContainer: false)
        }
        findtopViewController()?.navigationController?.pushViewController(objTab, animated: true)
    }
    
    @IBAction func btnSideMenuAction(_ sender: UIButton) {
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        drawerController?.openSide(.left)
    }
}
