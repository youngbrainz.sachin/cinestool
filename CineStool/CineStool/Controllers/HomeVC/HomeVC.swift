//
//  HomeVC.swift
//  CineStool
//
//  Created by YB on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import AVKit
import GoogleCast

class HomeVC: UIViewController {
    
    @IBOutlet weak var collectionHome: UICollectionView!
    @IBOutlet weak var TblHome: UITableView!
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var btnSearch: UIButton!

    var arrCategory = [""]
    var timer = Timer()
    var player = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionHome.isHidden = true
        TblHome.isHidden = true
        onloadOperation()
        setupDetail()
        apiCallGetCategoryList()
        apiCallGetDashboardData()
        startTimer()
    }
    
    func onloadOperation() {
        appDel.isCallAPI = true
        TblHome.register(HomeTblCell.self)
        collectionHome.register(HomeCollectionCell.self)
        btnSearch.tintColorDidChange()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIDevice.current.setValue(Int(UIInterfaceOrientationMask.portrait.rawValue), forKey: "orientation")
    }
    
    @objc func scrollToNextCell() {
        
        let cellSize = collectionHome.frame.size
        let contentOffset = collectionHome.contentOffset
        
        if collectionHome.contentSize.width <= collectionHome.contentOffset.x + cellSize.width
        {
            let r = CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            collectionHome.scrollRectToVisible(r, animated: false)
            
        } else {
            let r = CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            collectionHome.scrollRectToVisible(r, animated: true)
        }
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
//        UIView.animate(withDuration: 0.5) {
//            self.scrollToNextCell()
//            self.view.layoutIfNeeded()
//        }
    }
    
    @IBAction func btnSideMenuAction(_ sender: UIButton) {
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        drawerController?.openSide(.left)
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        let objTab = MovieListingVC(nibName: "MovieListingVC", bundle: nil)
        objTab.isFrom = .home
        self.navigationController!.pushViewController(objTab, animated: true)
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appDel.arrDashboardData != nil {
            let movie = appDel.arrDashboardData.categories.filter({$0.data.count != 0})
            return movie.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HomeTblCell = tableView.dequeueReusableCellTb(for: indexPath)
        let movie = appDel.arrDashboardData.categories.filter({$0.data.count != 0})
        cell.indexPath = indexPath
        cell.arrDashboardData = movie[indexPath.row].data
        cell.collectionHomeUnderTbl.reloadData()
        cell.lblCategory.text = movie[indexPath.row].name
        cell.HeightcollectionHomeUnderTbl.constant = cell.collectionHomeUnderTbl.collectionViewLayout.collectionViewContentSize.height
        cell.blockDidTapped = { index in
            appDel.redirectToMovieDetail(movieID: appDel.arrDashboardData.categories[indexPath.row].data[index]._id)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension //250
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if appDel.arrDashboardData != nil {
            return appDel.arrDashboardData.banner_movies.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HomeCollectionCell = collectionView.dequeueReusableCell(for: indexPath)
        let imgURL = appDel.arrDashboardData.banner_movies[indexPath.item].banner_image
        cell.imgPoster.sd_setImage(with: URL(string: imgURL), placeholderImage: #imageLiteral(resourceName: "HomeLogo"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        appDel.redirectToMovieDetail(movieID: self.arrDashboardData.banner_movies[indexPath.item]._id)
        redirectToMoviePlayer(openFrom: .Banner, index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControll.currentPage = indexPath.item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionHome.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

//MARK:- API CALL
extension HomeVC {
    
    func apiCallGetCategoryList() {
        /*
        required : [loginuser_id, session_token, user_type(1:Customer)]
        optional : [password,mobile_uid]
        */
        let params = ["mobile_uid":appDel.getDeviceUUID(),"password":appDel.Loginpassword] as [String : Any]
        WebService.Request.patch(url: getMovieCategoryV2, type: .post, parameter: params, callSilently : true) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [[String : Any]] {
                        let catData = ["name": "Home", "get_subcategory": [:]] as [String : Any]
                        var arrcat = [[String:Any]]()
                        arrcat.insert(catData, at: 0)
                        arrcat.append(["name": "Category", "get_subcategory": data])
                        arrcat.append(["name": "My Collection", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "History", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "Settings", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "My Profile", "get_subcategory": [:]] as [String : Any])
                        arrcat.append(["name": "Log Out", "get_subcategory": [:]] as [String : Any])
                        appDel.movieCategories.removeAll()
                        appDel.movieCategories = arrcat.map{Categories(JSON: $0)!}
                        print(appDel.movieCategories.count)
                    }
                } else {
//                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
//                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    
    func setupDetail(){
        if UserDefaults.standard.value(forKey: UserdefaultsConstants.homeData) != nil{
            let data = UserDefaults.standard.value(forKey: UserdefaultsConstants.homeData) as? [String:Any]
            appDel.arrDashboardData = DashboardModel(JSON: data!)!
            if let bannerMovie = data!["banner_movies"] as? [[String: Any]] {
                appDel.bannerMovies = bannerMovie.map{ BannerHome(JSON: $0)! }
            }
            self.pageControll.numberOfPages = appDel.arrDashboardData!.banner_movies.count
            self.collectionHome.reloadData()
            self.TblHome.reloadData()
            collectionHome.isHidden = false
            TblHome.isHidden = false
        }
    }
    
    func apiCallGetDashboardData() {
        let params = ["mobile_uuid":appDel.getDeviceUUID()] as [String : Any]
        var ishideLoader = true
        if appDel.arrDashboardData == nil{
            ishideLoader = false
        }
        WebService.Request.patch(url: getDashboardData, type: .post, parameter: params, callSilently : ishideLoader) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String: Any] {
                        UserDefaults.standard.set(data, forKey: UserdefaultsConstants.homeData)
                        UserDefaults.standard.synchronize()
                        self.setupDetail()
                        appDel.isAppLive = response!["is_live"] as? String ?? ""
                        appDel.badgeCount = Int(response!["badge_count"] as? String ?? "") ?? 0
                        NotificationCenter.default.post(name: .badge, object: nil, userInfo: ["badge": appDel.badgeCount])
                        let strSubscribed = response!["is_plan_subscribed"] as? String ?? "0"
                        if strSubscribed == "1"{
                            appDel.is_Subscribed = true
                        }else{
                            appDel.is_Subscribed = false
                            if appDel.isAppLive == appStatus.Live && appDel.isJustLoggedIn{
                                self.alertTwoButton(title: "Subscription", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Do you want to enroll in cinestool monthly subscription?") { (result) in
                                    UserDefaults.standard.set(false, forKey: UserdefaultsConstants.isJustLogin)
                                    UserDefaults.standard.synchronize()
                                    appDel.isJustLoggedIn = false
                                if result{
                                    let objPrivacyPolicyVC = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
                                    findtopViewController()?.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                                }else{
                                    UserDefaults.standard.set(true, forKey: UserdefaultsConstants.isPressedNoToSubscriptionPopup)
                                    appDel.isPressedNoToSubscriptionPopup = true
                                    UserDefaults.standard.synchronize()
                                    }
                                }
                            }
                        }
                    }
                    appDel.subscription_status = response!["subscription_status"] as? String ?? "3"
                    appDel.subscription_id = response!["subscription_id"] as? String ?? "0"
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
//                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
            WebService.Loader.hide()
        }
    }
    
    func redirectToMoviePlayer(openFrom: redirectToMoviePlayerFrom, index: NSInteger){
        let storyboard = UIStoryboard(name: "MoviePlayer", bundle: nil)
        let moviePlayer = storyboard.instantiateViewController(withIdentifier: "MoviePlayerVC") as? MoviePlayerVC
        moviePlayer?.modalPresentationStyle = .overFullScreen
        moviePlayer?.banner_movies = appDel.arrDashboardData.banner_movies[index]
        moviePlayer?.openFrom = openFrom
//        let castContainerVC =
//                GCKCastContext.sharedInstance().createCastContainerController(for: moviePlayer!)
//        castContainerVC.miniMediaControlsItemEnabled = true
        findtopViewController()?.navigationController!.pushViewController(moviePlayer!, animated: true)
    }
}

extension NSNotification.Name {
    static let badge = NSNotification.Name("badge")
}
