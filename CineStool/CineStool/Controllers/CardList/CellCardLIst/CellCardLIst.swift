//
//  CellCardLIst.swift
//  CineStool
//
//  Created by Dharam YB on 18/02/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class CellCardLIst: UITableViewCell, ReusableView, NibLoadableView {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblExpDate: UILabel!
    @IBOutlet weak var btnMakeDefault: UIButton!
    @IBOutlet weak var btnRadio: UIButton!
    
    var blockMakeDefault : ((Int)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnMakeDefaultTapped(_ sender: UIButton)
    {
        blockMakeDefault?(sender.tag)
    }
}
