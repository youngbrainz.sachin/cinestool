//
//  CardListVC.swift
//  CineStool
//
//  Created by Dharam YB on 18/02/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit
import AVFoundation
import ACFloatingTextfield_Swift
import IQKeyboardManagerSwift
import KWDrawerController

enum forSubscribe {
    case No
    case Yes
}

class CardListVC: UIViewController,UITextFieldDelegate {

    @IBOutlet var tblCardList: UITableView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnPayNow: UIButton!
    @IBOutlet var heighBtnPayNow: NSLayoutConstraint!
    
    @IBOutlet weak var viewMobileMoneyInfo: UIView!
    @IBOutlet weak var lblPopUpPrice: UILabel!
    
    @IBOutlet weak var viewInsertCVV: UIView!
    @IBOutlet weak var lblCVVTitle: UILabel!
    @IBOutlet weak var lblCVVDesc: UILabel!
    @IBOutlet weak var btnAddNewCard: UIButton!
    @IBOutlet weak var txtCVV: ACFloatingTextfield!
    
    var isFromSubscribe:forSubscribe = .No
    var index = 0
    var selectedSubscription = SubscriptionModel()
    var cardListing = [cardListingModel]()
    var paymentType = "2"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnAddNewCard.isHidden = true
        viewInsertCVV.isHidden = true
        self.txtCVV.isSecureTextEntry = true
        self.txtCVV.keyboardType = .numberPad
        self.tblCardList.register(CellCardLIst.self)
        tblCardList.tableFooterView = UIView()
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        if isFromSubscribe == .Yes{
            let price = Float(self.selectedSubscription?.price ?? "0.0")! * Float(self.selectedSubscription?.plan_number ?? "0.0")!
            lblPopUpPrice.text = "\(currencyCode ?? "$")\(String(format: "%.2f", price))"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.isFromSubscribe == .No{
            self.lblTitle.text = "CARD LIST"
            self.btnPayNow.isHidden = true
            self.heighBtnPayNow.constant = 0
        }else{
            self.lblTitle.text = "SUBSCRIBE"
            self.btnPayNow.isHidden = false
            self.heighBtnPayNow.constant = 40
        }
        self.apiCallGetCard()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSquareTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddTapped(_ sender: UIButton) {
        let objTab = AddEditCardVC(nibName: "AddEditCardVC", bundle: nil)
        objTab.isFrom = .add
        self.navigationController!.pushViewController(objTab, animated: true)
    }
    @IBAction func btnAddNewCard(_ sender: UIButton) {
        let objTab = AddEditCardVC(nibName: "AddEditCardVC", bundle: nil)
        objTab.isFrom = .add
        self.navigationController!.pushViewController(objTab, animated: true)
    }
    
    @IBAction func btnPayNowTapped(_ sender: UIButton) {
        if index == 0{
            self.viewMobileMoneyInfo.isHidden = false
        }else{
//            alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to pay?") { (status) in
//                if status{
                    let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
                    let codeInfo = countryInfo["country"] as! String
                    let currencyCodedetail = Locale.currencies[codeInfo]
                    let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
                    
                    let strCard = self.cardListing[self.index - 1].card_number.replace_fromStart(str: self.cardListing[self.index - 1].card_number, endIndex: 12, With: "************")
                    if self.isFromSubscribe == .Yes{
                        let price = Float(self.selectedSubscription?.price ?? "0.0")! * Float(self.selectedSubscription?.plan_number ?? "0.0")!
                        self.lblCVVTitle.text = "\(self.selectedSubscription?.plan ?? "") (\(currencyCode ?? "$")\((String(format: "%.2f", price))))"
                    }
                    self.lblCVVDesc.text = "Please insert CVV for this card: \(strCard)"
                    self.viewInsertCVV.isHidden = false
//                }
//            }
        }
    }
    
    @IBAction func btnHideCVVView(_ sender: UIButton) {
        self.view.endEditing(true)
        txtCVV.text = ""
        viewInsertCVV.isHidden = true
    }
    
    @IBAction func btnCancelCVV(_ sender: UIButton) {
        self.view.endEditing(true)
        txtCVV.text = ""
        viewInsertCVV.isHidden = true
    }

    @IBAction func btnPayCVV(_ sender: UIButton) {
        self.view.endEditing(true)
        viewInsertCVV.isHidden = true
        if txtCVV.text == ""{
            alertOk(title: "", message: "Please insert CVV.")
        }else{
            apiCallMakePaymentForSubscription(amount: "\(self.selectedSubscription?.actual_amount ?? "")", PaypalTransId: "", cvv: txtCVV.text ?? "")
            txtCVV.text = ""
        }
    }
    
    @IBAction func btnViewMobileMoneyHideAction(_ sender: UIButton) {
        self.viewMobileMoneyInfo.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCVV {
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else {
            return true
        }
    }
}

extension CardListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isFromSubscribe == .No{
            return self.cardListing.count
        }else{
            return self.cardListing.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CellCardLIst = tableView.dequeueReusableCellTb(for: indexPath)
        cell.btnMakeDefault.tag = indexPath.row
        
        if indexPath.row == self.index{
            cell.btnRadio.setImage(#imageLiteral(resourceName: "RadioDisable"), for: .normal)
        }else{
            cell.btnRadio.setImage(#imageLiteral(resourceName: "RadioUnable"), for: .normal)
        }
        
        if self.isFromSubscribe == .No{
            cell.btnMakeDefault.isHidden = false
            cell.btnRadio.isHidden = true
            cell.lblExpDate.text = cardListing[indexPath.row].expiry_date
            if self.cardListing[indexPath.row].is_default_card == "1"{
                cell.btnMakeDefault.isHidden = true
            }
            
            cell.blockMakeDefault = { tag in
                print(tag)
                self.apiCallMakeDefaultCard(cardId: self.cardListing[tag]._id, indexSelected: tag)
            }
            cell.lblName.isHidden = false
            cell.lblNo.isHidden = false
            cell.lblExpDate.text = cardListing[indexPath.row].expiry_date
            cell.lblName.text = cardListing[indexPath.row].holder_name
            
            cell.lblNo.text = cardListing[indexPath.row].card_number.replace_fromStart(str: cardListing[indexPath.row].card_number, endIndex: 12, With: "************")


        }else{
            cell.btnMakeDefault.isHidden = true
            cell.btnRadio.isHidden = false
            if indexPath.row == 0{
                cell.lblName.isHidden = true
                cell.lblNo.isHidden = true
                cell.lblExpDate.text = "Mobile Money"
            }else{
                cell.lblName.isHidden = false
                cell.lblNo.isHidden = false
                cell.lblExpDate.text = cardListing[indexPath.row - 1].expiry_date
                cell.lblName.text = cardListing[indexPath.row - 1].holder_name
                cell.lblNo.text = cardListing[indexPath.row - 1].card_number.replace_fromStart(str: cardListing[indexPath.row - 1].card_number, endIndex: 12, With: "************")
            }
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isFromSubscribe == .No{
            return UITableView.automaticDimension
        }else{
            if indexPath.row == 0{
                return 55
            }else{
                return UITableView.automaticDimension
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        if self.isFromSubscribe == .No && self.cardListing.count > 1{
//            return true
//        }else{
//            return false
//        }
        if self.isFromSubscribe == .No{
            if self.cardListing.count > 1{
                return true
            }else{
                return false
            }
        }else{
            if indexPath.row == 0{
                return false
            }else if self.cardListing.count > 1{
                return true
            }else{
                return false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.alertTwoButton(title: "CineStool", message: "Are you sure you want to Delete?") { (result) in
                if result {
                    self.apiCallDeleteCard(cardId: self.cardListing[indexPath.row]._id, indexSelected: indexPath.row)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isFromSubscribe == .No{
            let objTab = AddEditCardVC(nibName: "AddEditCardVC", bundle: nil)
            objTab.isFrom = .edit
            objTab.name = self.cardListing[indexPath.row].holder_name
            objTab.cardNumber = self.cardListing[indexPath.row].card_number
            objTab.date = self.cardListing[indexPath.row].expiry_date
            objTab.cvv = self.cardListing[indexPath.row].cvv
            objTab.card_id = self.cardListing[indexPath.row]._id
            self.navigationController!.pushViewController(objTab, animated: true)
        }else{
            self.index = indexPath.row
            self.tblCardList.reloadData()
        }
    }
    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
//            print("index path of delete: \(indexPath)")
//            completionHandler(true)
//        }
//
//        let rename = UIContextualAction(style: .normal, title: "Edit") { (action, sourceView, completionHandler) in
//            print("index path of edit: \(indexPath)")
//            completionHandler(true)
//        }
//        let swipeActionConfig = UISwipeActionsConfiguration(actions: [rename, delete])
//        swipeActionConfig.performsFirstActionWithFullSwipe = false
//        return swipeActionConfig
//    }
}

extension CardListVC{
    func apiCallGetCard() {
        WebService.Request.patch(url: getUserCard, type: .post, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    if let data = response!["data"] as? [[String: Any]] {
                        self.cardListing = data.map{cardListingModel(JSON: $0)!}
                        self.tblCardList.reloadData()
                        if self.cardListing.count == 0{
                            self.btnAddNewCard.isHidden = false
                            self.btnPayNow.isHidden = true
                        }else{
                            self.btnAddNewCard.isHidden = true
                            self.btnPayNow.isHidden = false
                        }
                    }else{
                        
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallMakeDefaultCard(cardId: String, indexSelected: NSInteger) {
        let param = ["card_id": cardId]
        WebService.Request.patch(url: makeDefaultUserCard, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    let itemFirst = self.cardListing[0]
                    itemFirst.is_default_card = "0"
                    let itemSelected = self.cardListing[indexSelected]
                    itemSelected.is_default_card = "1"
                    self.cardListing[0] = itemSelected
                    self.cardListing[indexSelected] = itemFirst
                    self.tblCardList.reloadData()
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallDeleteCard(cardId: String, indexSelected: NSInteger) {
        let param = ["card_id": cardId]
        WebService.Request.patch(url: deleteUserCard, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                        self.cardListing.remove(at: indexSelected)
                        self.tblCardList.reloadData()
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallMakePaymentForSubscription(amount: String, PaypalTransId: String, cvv: String) {
        var price = Float()
        if isFromSubscribe == .Yes{
            price = Float(self.selectedSubscription?.actual_amount ?? "0.0")! * Float(self.selectedSubscription?.plan_number ?? "0.0")!
        }

        var param = [String:Any]()
        param = ["payment_type" : paymentType,
                 "amount" : String(format: "%.2f", price),
                 "subscription_id": self.selectedSubscription?._id ?? ""]
        if paymentType == "1" {
            param["transactions_id"] = PaypalTransId
        } else if paymentType == "2" {
            param["card_number"] = "\(self.cardListing[self.index - 1].card_number)"
            param["cvv"] = "\(cvv)"
            param["expiry_month"] = "\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").first
            param["expiry_year"] = "\(self.cardListing[self.index - 1].expiry_date)".components(separatedBy: "/").last
        } else {
            
        }
        WebService.Request.patch(url: SubscribedUserPayment, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "OK", completion: { (Bool) in
                        let mainViewController = MainContainerVC(nibName: "MainContainerVC", bundle: .main)
                        let menuViewController = SideMenuVC(nibName: "SideMenuVC", bundle: .main)
                        //Set for callback use
                        appDel.sideMenuVC = menuViewController
                        
                        let aDrawer = DrawerController()
                        aDrawer.setViewController(mainViewController, for: .none)
                        aDrawer.setViewController(menuViewController, for: .left)
                        
                        aDrawer.drawerWidth = Float((UIScreen.main.bounds.size.width / 3) * 2) + 30
        //                let castContainerVC =
        //                        GCKCastContext.sharedInstance().createCastContainerController(for: aDrawer)
        //                castContainerVC.miniMediaControlsItemEnabled = true

                        self.navigationController?.pushViewController(aDrawer, animated: true)

                    })
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}

extension String
{
    func group(by groupSize:Int=3, separator:String="-") -> String{
         if self.count <= groupSize   { return self }
         let splitSize  = min(max(1,self.count-1) , groupSize)
         let splitIndex = index(startIndex, offsetBy:splitSize)
         return substring(to:splitIndex)
              + separator
              + substring(from:splitIndex).group(by:groupSize, separator:separator)
      }
    func replace_fromStart(str:String , endIndex:Int , With:String) -> String {
        var strReplaced = str ;
        let start = str.startIndex;
        let end = str.index(str.startIndex, offsetBy: endIndex);
        strReplaced = str.replacingCharacters(in: start..<end, with: With) ;
        return strReplaced;
    }
}
