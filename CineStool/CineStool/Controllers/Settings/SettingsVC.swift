//
//  SettingsVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import LocalAuthentication

class SettingsVC: UIViewController {
    
    //MARK:- Variables & Outlets
    @IBOutlet weak var tblSettings: UITableView!
    
    var arrOption = [""]
    var isOn = false
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        appDel.isCallAPI = true
        self.tblSettings.register(CellSettings.self)
        self.tblSettings.tableFooterView = UIView()
        self.setSettingOption()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSquareTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        guard drawerController?.drawerSide != .left else {
            drawerController?.closeSide()
            return
        }
        drawerController?.openSide(.left)
    }
}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CellSettings = tableView.dequeueReusableCellTb(for: indexPath)
        cell.lblTitle.text = self.arrOption[indexPath.item]
        
        if appDel.subscription_status == "2"{
            if indexPath.row == 1 {
                cell.btnSwitch.isHidden = false
                if appDel.loggedInUserData?.notification_status == "1"{
                    cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch On"), for: .normal)
                }else{
                    cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch Off"), for: .normal)
                }
            } else if indexPath.row == 3 {
                cell.btnSwitch.isHidden = false
                if chekTouchId(){
                    if UserDefaults.standard.bool(forKey: "isTouchID") == true {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch On"), for: .normal)
                    }else {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch Off"), for: .normal)
                    }
                }else{
                    cell.btnSwitch.isHidden = true
                }
            } else{
                cell.btnSwitch.isHidden = true
            }
        }else{
            if indexPath.row == 2 {
                cell.btnSwitch.isHidden = false
                if appDel.loggedInUserData?.notification_status == "1"{
                    cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch On"), for: .normal)
                }else{
                    cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch Off"), for: .normal)
                }
            } else if indexPath.row == 4 {
                cell.btnSwitch.isHidden = false
                if chekTouchId(){
                    if UserDefaults.standard.bool(forKey: "isTouchID") == true {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch On"), for: .normal)
                    }else {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch Off"), for: .normal)
                    }
                }else{
                    cell.btnSwitch.isHidden = true
                }
            } else{
                cell.btnSwitch.isHidden = true
            }
        }
        
        cell.btnSwitch.tag = indexPath.row
        cell.blockSwitchChanged = { index in
            print(index)
            
            if appDel.subscription_status == "2"{
                if index == 1 {
                    if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Switch Off") {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch On"), for: .normal)
                        self.apiCallForUpdateProfile(status: statusActiveInactive.Active)
                    } else {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch Off"), for: .normal)
                        self.apiCallForUpdateProfile(status: statusActiveInactive.InActive)
                    }
                } else if index == 3 {
                    if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Switch Off") {
                        authenticationWithTouchID(completion: {
                            DispatchQueue.main.async {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch On"), for: .normal)
                                UserDefaults.standard.set(true, forKey: "isTouchID")
                            }
                        })
                    } else {
                        authenticationWithTouchID(completion: {
                            DispatchQueue.main.async {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch Off"), for: .normal)
                                UserDefaults.standard.set(false, forKey: "isTouchID")
                            }
                        })
                    }
                }
            }else{
                if index == 2 {
                    if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Switch Off") {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch On"), for: .normal)
                        self.apiCallForUpdateProfile(status: statusActiveInactive.Active)
                    } else {
                        cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch Off"), for: .normal)
                        self.apiCallForUpdateProfile(status: statusActiveInactive.InActive)
                    }
                } else if index == 4 {
                    if cell.btnSwitch.currentImage == #imageLiteral(resourceName: "Switch Off") {
                        authenticationWithTouchID(completion: {
                            DispatchQueue.main.async {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch On"), for: .normal)
                                UserDefaults.standard.set(true, forKey: "isTouchID")
                            }
                        })
                    } else {
                        authenticationWithTouchID(completion: {
                            DispatchQueue.main.async {
                                cell.btnSwitch.setImage(#imageLiteral(resourceName: "Switch Off"), for: .normal)
                                UserDefaults.standard.set(false, forKey: "isTouchID")
                            }
                        })
                    }
                }
            }
        }
        return cell
    }
    
    func chekTouchId() -> Bool {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
        var authError: NSError?
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            if #available(iOS 11.0, *) {
                switch (localAuthenticationContext.biometryType){
                case .none:
                    return false
                case .touchID:
                    return true
                case .faceID:
                    return true
                default: break
                }
            } else {
                // Fallback on earlier versions
                return false
            }
        } else {
            return false
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if appDel.isAppLive == appStatus.Live{
            if appDel.subscription_status == "2"{
                if indexPath.row == 0{
                    let objTab = PaymentHistoryVC(nibName: "PaymentHistoryVC", bundle: nil)
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else if indexPath.row == 1{
                    
                }else if indexPath.row == 2{
                    let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                    objTab.isFromSubscribe = .No
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else if indexPath.row == 3 && chekTouchId(){
                    
                }else if indexPath.row == 3 && !chekTouchId() || indexPath.row == 4 && chekTouchId(){
                    let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                    objPrivacyPolicyVC.commonFile = .PrivacyPolicy;
                    self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                }else if indexPath.row == 4 && !chekTouchId() || indexPath.row == 5 && chekTouchId(){
                    let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                    objPrivacyPolicyVC.commonFile = .TermsAndCondition; self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                }else if indexPath.row == 5 && !chekTouchId() || indexPath.row == 6 && chekTouchId(){
                    let strMessage = "Do you really want to unsubscribe?"
                    alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: strMessage) { (status) in
                        if status{
                            self.apiCallUnSubscription()
                        }
                    }
                }
            }else{
                if indexPath.row == 0{
                    let objTab = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else if indexPath.row == 1{
                    let objTab = PaymentHistoryVC(nibName: "PaymentHistoryVC", bundle: nil)
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else if indexPath.row == 2{
                    
                }else if indexPath.row == 3{
                    let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                    objTab.isFromSubscribe = .No
                    self.navigationController!.pushViewController(objTab, animated: true)
                }else if indexPath.row == 4 && !chekTouchId() || indexPath.row == 5 && chekTouchId(){
                    let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                    objPrivacyPolicyVC.commonFile = .PrivacyPolicy;
                    self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                }else if indexPath.row == 5 && !chekTouchId() || indexPath.row == 6 && chekTouchId(){
                    let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                    objPrivacyPolicyVC.commonFile = .TermsAndCondition; self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                }
            }
        }else{
            if indexPath.row == 0{
                
            }else if indexPath.row == 1{
                let objPrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                objPrivacyPolicyVC.commonFile = .PrivacyPolicy;
                self.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 54
    }
}


//MARK:- API CALL
extension SettingsVC{
    
    func apiCallForUpdateProfile(status: String) {
        let params = ["status_type":statusType.notificatioStatus, "status":status] as [String : Any]
        WebService.Request.patch(url: changeUserStatus, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        let detail = data.nullKeyRemoval()
                        appDel.loggedInUserData = User (JSON: data)
                        UserDefaults.standard.set(detail, forKey: UserdefaultsConstants.userData)
                        UserDefaults.standard.synchronize()
                        appDel.setupProfile()
                    }
                } else {
                }
            } else {
            }
        }
    }
    
    func apiCallUnSubscription() {
        let param = ["subscription_status":appDel.subscription_status, "subscription_id":appDel.subscription_id]
        WebService.Request.patch(url: changeSubscribedPlan, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    appDel.subscription_status = response!["subscription_status"] as? String ?? "3"
                    self.setSettingOption()
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func setSettingOption(){
        if appDel.isAppLive == appStatus.Live{
            if chekTouchId(){
                if appDel.subscription_status == "2"{
                    self.arrOption = ["Payment History", "Push Notification","Payment Method", "Enable TouchID", "Privacy Policy","Terms & Conditions","UnSubscribe"]
                }else{
                    self.arrOption = ["Subscription Plan", "Payment History", "Push Notification","Payment Method", "Enable TouchID", "Privacy Policy","Terms & Conditions"]
                }
            }else{
                if appDel.subscription_status == "2"{
                    self.arrOption = ["Payment History", "Push Notification","Payment Method", "Privacy Policy","Terms & Conditions","UnSubscribe"]
                }else{
                    self.arrOption = ["Subscription Plan", "Payment History", "Push Notification","Payment Method", "Privacy Policy","Terms & Conditions"]
                }
            }
        }else{
            self.arrOption = ["Push Notification","Privacy Policy"]
        }
        self.tblSettings.reloadData()
    }
}


