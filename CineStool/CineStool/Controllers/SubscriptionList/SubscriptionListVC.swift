//
//  SubscriptionListVC.swift
//  CineStool
//
//  Created by Dharam YB on 17/02/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class SubscriptionListVC: UIViewController {

    @IBOutlet var tblSubscription: UITableView!
    @IBOutlet var btnSubscribe: UIButton!
    var index = Int()
    var subscriptionList = [SubscriptionModel]()
    
    @IBOutlet var btnCancelSubscription: UIButton!
    
    @IBOutlet var viewSubDetail: UIView!
    @IBOutlet var lblCurrSub: UILabel!
    @IBOutlet var heightviewSubDetail: NSLayoutConstraint!
    
    var subscriptionStatus = String()
    var selectedSubscriptionID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSubscription.register(UINib(nibName: "CellSubscription", bundle: .main), forCellReuseIdentifier: "CellSubscription")
        tblSubscription.tableFooterView = UIView()
        self.index = 0
        self.apiCallGetSubscriptionList()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnSquareTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubscribeTapped(_ sender: UIButton) {
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        alertTwoButton(title: "", titleButtonAccept: "Subscribe", titleButtonReject: "Cancel", message: "By subscribing, you agree to Cinestool monthly automatic billing and subscription terms on this platform.") { (result) in
            if result{
                print("Done")                
//                let objTab = PaymentVC(nibName: "PaymentVC", bundle: nil)
//                objTab.isFrom = self.isFrom
//                objTab.movieID = movieID
//                objTab.price = self.price
//                objTab.subscriptionPrice = subscriptionPrice
//                objTab.actual_symbol = actual_symbol
//                objTab.actual_price = self.actual_price
//                self.present(objTab, animated: true, completion: nil)
                
                let objTab = CardListVC(nibName: "CardListVC", bundle: nil)
                objTab.isFromSubscribe = .Yes
                objTab.selectedSubscription = self.subscriptionList[self.index]
                self.navigationController!.pushViewController(objTab, animated: true)
            }
        }
    }
    
    @IBAction func btnCancelSubscribeTapped(_ sender: UIButton) {
        print("Cancel")
        
        var strMessage = ""
        if subscriptionStatus == "2"{
            strMessage = "Are you sure you want to unsubscribe current subscription."
        }else{
            strMessage = "Are you sure you want to auto subscribe your current subscription."
        }
        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: strMessage) { (status) in
            if status{
                self.apiCallUnSubscription(subscription_status: self.subscriptionStatus)
            }
        }
    }
}

extension SubscriptionListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.subscriptionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell: CellSubscription = tableView.dequeueReusableCellTb(for: indexPath)
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        
        cell.lblTitle.text = "\(self.subscriptionList[indexPath.row].plan) (\(currencyCode ?? "$")\(self.subscriptionList[indexPath.row].price)/month)"
        if self.subscriptionList[indexPath.row].is_selected_plan == "1"{
            cell.btnCheck.setImage(#imageLiteral(resourceName: "RadioDisable"), for: .normal)
            self.index = indexPath.row
        }else{
            cell.btnCheck.setImage(#imageLiteral(resourceName: "RadioUnable"), for: .normal)
        }        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<self.subscriptionList.count{
            self.subscriptionList[i].is_selected_plan = "0"
        }
        self.subscriptionList[indexPath.row].is_selected_plan = "1"
        self.index = indexPath.row
        self.tblSubscription.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    
    func apiCallGetSubscriptionList() {
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let param = ["currency_code": "\(currencyCodedetail?.code ?? "USD")"]
        WebService.Request.patch(url: getSubscriptionList, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    if let data = response!["data"] as? [[String: Any]] {
                        self.subscriptionList = data.map{ SubscriptionModel(JSON: $0)!}
                        self.tblSubscription.reloadData()
                        if self.subscriptionList.count == 0{
                            self.btnSubscribe.isHidden = true
                        }else{
                            self.btnSubscribe.isHidden = false
                        }
                        self.selectedSubscriptionID = response!["subscription_id"] as? String ?? ""
                        self.heightviewSubDetail.constant -= self.viewSubDetail.frame.size.height//0
                        self.lblCurrSub.text = response!["subscription_msg"] as? String ?? ""
                        self.subscriptionStatus = response!["subscription_status"] as? String ?? "2"
                        self.btnCancelSubscription.isHidden = false
                        if self.subscriptionStatus == "1"{
                            self.btnCancelSubscription.setTitle("Auto Subscription", for: .normal)
                        }else if self.subscriptionStatus == "2"{
                            self.btnCancelSubscription.setTitle("UnSubscribe", for: .normal)
                        }else{
                            self.btnCancelSubscription.isHidden = true
                        }
//                        if response!["is_auto_subscribed"] as? String == "1"{
//                        }else{
//                            self.viewSubDetail.isHidden = true
//                            self.heightviewSubDetail.constant -= self.viewSubDetail.frame.size.height
//                            self.subscriptionStatus = response!["is_auto_subscribed"] as? String ?? "2"
//                        }
                        
                    }else{
                        
                    }
                } else {
                    self.navigationController?.popViewController(animated: true)
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.navigationController?.popViewController(animated: true)
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallUnSubscription(subscription_status:String) {
        
        //subscription_status : 1 : subscription, 2 : unsubscription
        let param = ["subscription_status":subscriptionStatus, "subscription_id":selectedSubscriptionID]
        WebService.Request.patch(url: changeSubscribedPlan, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    print("-->",response!)
                    self.heightviewSubDetail.constant -= self.viewSubDetail.frame.size.height//0
//                     self.selectedSubscriptionID = response!["subscription_id"] as? String ?? ""
                    self.lblCurrSub.text = response!["subscription_msg"] as? String
                    self.subscriptionStatus = response!["subscription_status"] as? String ?? "2"
                    self.btnCancelSubscription.isHidden = false
                    if self.subscriptionStatus == "1"{
                        self.btnCancelSubscription.setTitle("Auto Subscription", for: .normal)
                    }else if self.subscriptionStatus == "2"{
                        self.btnCancelSubscription.setTitle("UnSubscribe", for: .normal)
                    }else{
                        self.btnCancelSubscription.isHidden = true
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}
