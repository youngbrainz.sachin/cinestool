//
//  AddEditCardVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 23/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import IQKeyboardManagerSwift

enum cardType {
    case none
    case add
    case edit
}

class AddEditCardVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var txtCardName: ACFloatingTextfield!
    @IBOutlet weak var txtCardNumber: ACFloatingTextfield!
    @IBOutlet weak var txtCardExpiry: ACFloatingTextfield!
    @IBOutlet weak var txtCardCVV: ACFloatingTextfield!
    
    @IBOutlet weak var viewDatePick: UIView!
    @IBOutlet weak var viewMonthYearPicker: MonthYearPickerView!
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnAddEdit: UIButton!
    
    var isFrom:cardType = .none
    
    var name = String()
    var cardNumber = String()
    var cvv = String()
    var card_id = String()
    var date = String()
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        hideView()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        self.txtCardCVV.isSecureTextEntry = true
        if isFrom == .add{
            self.lblHeaderTitle.text = "Add Card"
            self.btnAddEdit.setTitle("ADD CARD", for: .normal)
        }else if isFrom == .edit{
            self.lblHeaderTitle.text = "Update Card"
            self.btnAddEdit.setTitle("UPDATE CARD", for: .normal)
            
            self.txtCardName.text = self.name
            self.txtCardNumber.text = self.cardNumber
            self.txtCardExpiry.text = self.date
            self.txtCardCVV.text = self.cvv
        }
    }
    
    func hideView() {
        viewDatePick.isHidden = true
    }
    
    func ShowView() {
        viewDatePick.isHidden = false
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnShowDatePicker(_ sender: UIButton) {
        self.view.endEditing(true)
        ShowView()
    }
    
    @IBAction func btnHideDatePicker(_ sender: UIButton) {
        let string = String(format: "%02d/%d", viewMonthYearPicker.month, viewMonthYearPicker.year)
        
        let date1Format = DateFormatter()
        date1Format.dateFormat = "MM/yyyy"
        let date1 = date1Format.date(from: string)
        
        let now = Date()
        let currDate = date1Format.string(from: now)
        let date2 = date1Format.date(from: currDate)
        
        if date1! < date2! {
            alertOk(title: "Cinestool", message: "Invalid Expiry Date")
            self.txtCardExpiry.text = ""
        }else{
            self.txtCardExpiry.text = string
        }
        
//        if date1! < date2! {
//            print("leftDate is earlier than rightDate")
//        } else if date1! > date2! {
//            print("leftDate is later than rightDate")
//        } else if date1! == date2! {
//            print("dates are equal")
//        }
        hideView()
    }
    
    @IBAction func btnAddEditAction(_ sender: UIButton) {
        if validate() {
            self.apiCallAddEditCard()
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddEditCardVC: UITextFieldDelegate
{
    //Validate Fields
    func validate() -> Bool
    {
        if txtCardName.text == "" {
            alertOk(title: "", message: InsertCardName) { (result) in
            }
            return false
        }
        else if txtCardNumber.text == "" {
            alertOk(title: "", message: InsertCardNumber) { (result) in
            }
            return false
        }
        else if txtCardExpiry.text == "" {
            alertOk(title: "", message: InsertCardExpiryDate) { (result) in
            }
            return false
        }else if txtCardCVV.text == "" {
            alertOk(title: "", message: InsertCVV) { (result) in
            }
            return false
        }
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCardNumber || textField == txtCardNumber {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else if textField == txtCardCVV {
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension AddEditCardVC{
    func apiCallAddEditCard() {
        var param = [String:Any]()
        param["holder_name"] = self.txtCardName.text
        param["card_number"] = self.txtCardNumber.text
        param["expiry_date"] = self.txtCardExpiry.text
        param["cvv"] = self.txtCardCVV.text
        if isFrom == .edit{
            param["card_id"] = card_id
        }
        WebService.Request.patch(url: addUserCard, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "OK", completion: { (Bool) in self.navigationController?.popViewController(animated: true)
                    })
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}
