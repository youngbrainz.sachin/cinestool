//
//  MobileVerifyVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 10/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import LocalAuthentication

class MobileVerifyVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    @IBOutlet weak var viewCreatePassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfPassword: UITextField!
    @IBOutlet weak var heightConfPass: NSLayoutConstraint!
    @IBOutlet weak var heightShowConfPass: NSLayoutConstraint!
    @IBOutlet weak var btnCloseViewCreatePass: UIButton!
    @IBOutlet weak var btnShowConfPass: UIButton!
    
    @IBOutlet weak var txtViewInfo: UITextView!
    @IBOutlet weak var btnCreatePassword: UIButton!
    
    let InsertPassword = "Insert Password"
    let InsertConfirmPassword = "Insert Confirm Password"
    let PasswordNotMatched = "Password & Confirm Password Not Matched"
    
    var isCheckPassword = false
    
    var mobile_money_video = ""
    var signup_video = ""
    var str_instruction = ""
    var signup_video_status = "0"
    var mobile_money_video_status = "0"
    var mob_status_text = "0"

    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        setupCurrentCountryDialingCode()
    }
    
    func setupCurrentCountryDialingCode() {
        if UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String != nil && UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String != nil{
            
            if  UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String == ""{
                let temp = Common.getCountryList()
                for item in temp{
                    if item["country"] as? String == Locale.current.regionCode{
                        self.lblCountryCode.text = item["code"] as? String ?? "+1"
                    }
                }
            }else{
                self.lblCountryCode.text = UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String ?? Locale.current.regionCode
            }
            self.txtMobile.text = UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String
        }else{
            let temp = Common.getCountryList()
            for item in temp{
                if item["country"] as? String == Locale.current.regionCode{
                    self.lblCountryCode.text = item["code"] as? String ?? "+1"
                }
            }
        }
        
        if self.lblCountryCode.text == "" || self.lblCountryCode.text == nil{
            let temp = Common.getCountryList()
            for item in temp{
                if item["country"] as? String == Locale.current.regionCode{
                    self.lblCountryCode.text = item["code"] as? String ?? "+1"
                }
            }
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnCountryCodeTapped(_ sender: UIButton) {
        var objCountryListVC: CountryListVC?
        objCountryListVC = CountryListVC(nibName: "CountryListVC", bundle: nil)
        objCountryListVC?.selectedCountry = {(countryData) in
           self.lblCountryCode.text = countryData["code"] as? String ?? ""
        }
        self.present(objCountryListVC!,animated: true)
    }
    
    @IBAction func btnLetsGoTapped(_ sender: UIButton) {
        if txtMobile.text == "" {
            alertController(message: "Insert Mobile Number", controller: self)
        }else{
            if (self.txtMobile.text == UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String) && (self.lblCountryCode.text == UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String) &&    (UserDefaults.standard.bool(forKey: "isTouchID") == true) {
                apiCallForLoginRegister(UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileNumber) as? String ?? "", code: UserDefaults.standard.value(forKey: UserdefaultsConstants.mobileCode) as? String ?? "")
            }else{
                self.apiCallForLoginRegister(self.txtMobile.text ?? "", code: self.lblCountryCode.text ?? Locale.current.regionCode ?? "")
                //self.APICallgenerateOTP(self.txtMobile.text ?? "", code: self.lblCountryCode.text ?? Locale.current.regionCode ?? "")
            }
        }
    }
    
    @IBAction func btnForgotTapped(_ sender: UIButton) {
        let objEmailVC = EmailVerifyVC(nibName: "EmailVerifyVC", bundle: nil)
        objEmailVC.isfrom = .fromForgotMobile
        self.navigationController?.pushViewController(objEmailVC, animated: true)
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        let objEmailVC = EmailVerifyVC(nibName: "EmailVerifyVC", bundle: nil)
        objEmailVC.isfrom = .fromForgotPassword
        self.navigationController?.pushViewController(objEmailVC, animated: true)
    }
    
    @IBAction func btnHideCreatePassViewTapped(_ sender: UIButton) {
        self.viewCreatePassword.isHidden = true
        self.view.endEditing(true)
        self.heightConfPass.constant = 50
        self.heightShowConfPass.constant = 40
        
        self.txtPassword.text = ""
        self.txtConfPassword.text = ""
    }
    
    @IBAction func btnShowPassTapped(_ sender: UIButton) {
        if self.txtPassword.isSecureTextEntry {
           self.txtPassword.isSecureTextEntry = false
        }else{
            self.txtPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnShowConfPassTapped(_ sender: UIButton) {
        if self.txtConfPassword.isSecureTextEntry {
           self.txtConfPassword.isSecureTextEntry = false
        }else{
            self.txtConfPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnCreatePassTapped(_ sender: UIButton) {
        if appDel.loggedInUserData?.is_password_added == "1"{
            if txtPassword.text == "" || txtPassword.text?.isEmpty == true{
                alertController(message: InsertPassword, controller: self)
            }else{
                if appDel.loggedInUserData?.password != ""{
                    if txtPassword.text != appDel.loggedInUserData?.password{
                        alertController(message: "Password does not match, Please try again later!", controller: self)
                    }else{
                        self.isCheckPassword = true
                        self.apiCallInstruction()
                    }
                }else{
                    self.isCheckPassword = false
                    self.apiCallInstruction()
                }
            }
        }else{
            if txtPassword.text == "" || txtPassword.text?.isEmpty == true{
                alertController(message: InsertPassword, controller: self)
            }else if txtConfPassword.text == "" || txtConfPassword.text?.isEmpty == true{
                alertController(message: InsertConfirmPassword, controller: self)
            }else if txtPassword.text != txtConfPassword.text {
                alertController(message: PasswordNotMatched, controller: self)
            }else{
                self.alertTwoButton(title: "Biometric Authentication", titleButtonAccept: "OK", titleButtonReject: "CANCEL", message: "Do you want to enableBiometric Authenticate") { (status) in
                    if status{
                        if self.chekTouchId(){
                            let objTab = BiometricAuthVC(nibName: "BiometricAuthVC", bundle: nil)
                            objTab.isFrom = .mobileVerify
                            objTab.authSuccess = {
                                self.isCheckPassword = false
                                self.apiCallInstruction()
                            }
                            self.present(objTab, animated: true, completion: nil)
                        }else{
                            self.isCheckPassword = false
                            self.apiCallInstruction()
                        }
                    }else{
                        self.isCheckPassword = false
                        self.apiCallInstruction()
                    }
                }
            }
        }
    }
}

extension MobileVerifyVC : UITextFieldDelegate {
    //MARK:- TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtMobile
        {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }else {
            return true
        }
    }
}

extension MobileVerifyVC{
    func apiCallForLoginRegister(_ mobile:String,code:String) {
        let countryInfo = getCountryCodeFromDialcode(dialCode: code)
        let codeInfo = countryInfo["country"] as! String
        let currencyCode = Locale.currencies[codeInfo]
        let currencyCodeSymbol = getCurrencySymbol(forCurrencyCode: currencyCode?.code ?? "USD")
        
        let params = ["device_type":"1",
                      "user_type":appDel.userType,
                      "device_token": appDel.deviceToken,
                      "mobile_no": mobile,
                      "country_code": code,
                      "country_code_info": codeInfo,
                      "currency_code": currencyCode?.code ?? "USD",
                      "currency_symbol": currencyCodeSymbol ?? "$",
                      "mobile_uid":appDel.getDeviceUUID()] as [String : Any]
        //loginRegister
        WebService.Request.patch(url: loginRegisterV2, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        let detail = data.nullKeyRemoval()
                        appDel.loggedInUserData = User (JSON: data)
                        UserDefaults.standard.set(detail, forKey: UserdefaultsConstants.userData)
                        UserDefaults.standard.set(appDel.loggedInUserData?.phone_number ?? "", forKey: UserdefaultsConstants.mobileNumber)
                        UserDefaults.standard.set(appDel.loggedInUserData?.country_code ?? "", forKey: UserdefaultsConstants.mobileCode)
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.set(true, forKey: UserdefaultsConstants.isJustLogin)
                        appDel.isJustLoggedIn = true
                        
                        UserDefaults.standard.set(false, forKey: UserdefaultsConstants.isPressedNoToSubscriptionPopup)
                        appDel.isPressedNoToSubscriptionPopup = false
                        UserDefaults.standard.synchronize()
                        
                        if appDel.loggedInUserData?.new_user == "1" {
                            self.APICallgenerateOTP(self.txtMobile.text ?? "", code: self.lblCountryCode.text ?? Locale.current.regionCode ?? "")
                        }else{
                            if appDel.loggedInUserData?.is_same_device == "1"{
                                print("Same Device")
                                if appDel.loggedInUserData?.is_password_added == "1"{
                                    self.alertTwoButton(title: "Biometric Authentication", titleButtonAccept: "OK", titleButtonReject: "CANCEL", message: "Do you want to enable Biometric Authenticate") { (status) in
                                        if status{
                                            if self.chekTouchId(){
                                                let objTab = BiometricAuthVC(nibName: "BiometricAuthVC", bundle: nil)
                                                objTab.isFrom = .mobileVerify
                                                objTab.authSuccess = {
                                                    //self.apiCallInstruction()
                                                    self.viewCreatePassword.isHidden = false
                                                    self.heightConfPass.constant = 0
                                                    self.heightShowConfPass.constant = 0
                                                    self.btnShowConfPass.isHidden = true
                                                    self.btnCloseViewCreatePass.isHidden = false
                                                    
                                                    self.txtViewInfo.text = "Enter your Valid Account Password"
                                                    self.txtPassword.placeholder = "Enter your Password to Login"
                                                    self.btnCreatePassword.setTitle("Login", for: .normal)
                                                }
                                                self.present(objTab, animated: true, completion: nil)
                                            }else{
                                                //self.apiCallInstruction()
                                                self.viewCreatePassword.isHidden = false
                                                self.heightConfPass.constant = 0
                                                self.heightShowConfPass.constant = 0
                                                self.btnShowConfPass.isHidden = true
                                                self.btnCloseViewCreatePass.isHidden = false
                                                
                                                self.txtViewInfo.text = "Enter your Valid Account Password"
                                                self.txtPassword.placeholder = "Enter your Password to Login"
                                                self.btnCreatePassword.setTitle("Login", for: .normal)
                                            }
                                        }else{
                                            self.viewCreatePassword.isHidden = false
                                            self.heightConfPass.constant = 0
                                            self.heightShowConfPass.constant = 0
                                            self.btnShowConfPass.isHidden = true
                                            self.btnCloseViewCreatePass.isHidden = false
                                            
                                            self.txtViewInfo.text = "Enter your Valid Account Password"
                                            self.txtPassword.placeholder = "Enter your Password to Login"
                                            self.btnCreatePassword.setTitle("Login", for: .normal)
                                            //& Watch Movies
                                        }
                                    }
                                }else{
                                    self.viewCreatePassword.isHidden = false
                                    self.heightConfPass.constant = 50
                                    self.heightShowConfPass.constant = 40
                                    self.btnShowConfPass.isHidden = false
                                    self.btnCloseViewCreatePass.isHidden = true
                                    
                                    self.txtViewInfo.text = "Create Password"
                                    self.txtPassword.placeholder = "Enter New Password"
                                    self.btnCreatePassword.setTitle("Login", for: .normal)
                                }
                            }else{
                                print("Not Same Device")
                                if appDel.loggedInUserData?.is_password_added == "1"{
                                    self.alertTwoButton(title: "Biometric Authentication", titleButtonAccept: "OK", titleButtonReject: "CANCEL", message: "Do you want to enableBiometric Authenticate") { (status) in
                                        if status{
                                            if self.chekTouchId(){
                                                let objTab = BiometricAuthVC(nibName: "BiometricAuthVC", bundle: nil)
                                                objTab.isFrom = .mobileVerify
                                                objTab.authSuccess = {
                                                    //self.apiCallInstruction()
                                                    self.viewCreatePassword.isHidden = false
                                                    self.heightConfPass.constant = 0
                                                    self.heightShowConfPass.constant = 0
                                                    self.btnShowConfPass.isHidden = true
                                                    self.btnCloseViewCreatePass.isHidden = false
                                                    
                                                    self.txtViewInfo.text = "Enter your Valid Account Password"
                                                    self.txtPassword.placeholder = "Enter your Password to Login"
                                                    self.btnCreatePassword.setTitle("Login", for: .normal)
                                                }
                                                self.present(objTab, animated: true, completion: nil)
                                            }else{
                                                //self.apiCallInstruction()
                                                self.viewCreatePassword.isHidden = false
                                                self.heightConfPass.constant = 0
                                                self.heightShowConfPass.constant = 0
                                                self.btnShowConfPass.isHidden = true
                                                self.btnCloseViewCreatePass.isHidden = false
                                                
                                                self.txtViewInfo.text = "Enter your Valid Account Password"
                                                self.txtPassword.placeholder = "Enter your Password to Login"
                                                self.btnCreatePassword.setTitle("Login", for: .normal)
                                            }
                                        }else{
                                            self.viewCreatePassword.isHidden = false
                                            self.heightConfPass.constant = 0
                                            self.heightShowConfPass.constant = 0
                                            self.btnShowConfPass.isHidden = true
                                            self.btnCloseViewCreatePass.isHidden = false
                                            
                                            self.txtViewInfo.text = "Enter your Valid Account Password"
                                            self.txtPassword.placeholder = "Enter your Password to Login"
                                            self.btnCreatePassword.setTitle("Login", for: .normal)
                                            //& Watch Movies
                                        }
                                    }
                                }else{
                                    self.viewCreatePassword.isHidden = false
                                    self.heightConfPass.constant = 50
                                    self.heightShowConfPass.constant = 40
                                    self.btnShowConfPass.isHidden = false
                                    self.btnCloseViewCreatePass.isHidden = true
                                    
                                    self.txtViewInfo.text = "Create Password"
                                    self.txtPassword.placeholder = "Enter New Password"
                                    self.btnCreatePassword.setTitle("Login", for: .normal)
                                }
                            }
                        }
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallInstruction() {
        
        appDel.Loginpassword = self.txtPassword.text ?? ""
        
        self.viewCreatePassword.isHidden = true
        self.view.endEditing(true)
        self.heightConfPass.constant = 50
        self.heightShowConfPass.constant = 40
        
        self.txtPassword.text = ""
        self.txtConfPassword.text = ""
        
        WebService.Request.patch(url: getCustomerInstruction, type: .get, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String: Any] {
                        self.mobile_money_video = data["mobile_money_video"] as! String
                        self.signup_video = data["signup_video"] as! String
                        self.str_instruction = data["mob_description"] as! String
                        self.signup_video_status = data["signup_video_status"] as! String
                        self.mobile_money_video_status = data["mobile_money_video_status"] as! String
                        self.mob_status_text = data["mob_status"] as! String
                    }
                }
            }
            
            if self.isCheckPassword{
                self.registrationInstructionFlow()
            }else{
                appDel.setupProfile()
                self.apiCallGetCategoryList()
            }
        }
    }
    
    func apiCallGetCategoryList() {
            /*
            required : [loginuser_id, session_token, user_type(1:Customer)]
            optional : [password,mobile_uid]
            */
            let params = ["mobile_uid":appDel.getDeviceUUID(),"password":appDel.Loginpassword] as [String : Any]
            WebService.Request.patch(url: getMovieCategoryV2, type: .post, parameter: params, callSilently : true) { (response, error) in
                if error == nil {
                    if response!["status"] as? Int == 1 {
                        if let data = response!["data"] as? [[String : Any]] {
                            let catData = ["name": "Home", "get_subcategory": [:]] as [String : Any]
                            var arrcat = [[String:Any]]()
                            arrcat.insert(catData, at: 0)
                            arrcat.append(["name": "Category", "get_subcategory": data])
                            arrcat.append(["name": "My Collection", "get_subcategory": [:]] as [String : Any])
                            arrcat.append(["name": "History", "get_subcategory": [:]] as [String : Any])
                            arrcat.append(["name": "Settings", "get_subcategory": [:]] as [String : Any])
                            arrcat.append(["name": "My Profile", "get_subcategory": [:]] as [String : Any])
                            arrcat.append(["name": "Log Out", "get_subcategory": [:]] as [String : Any])
                            appDel.movieCategories.removeAll()
                            appDel.movieCategories = arrcat.map{Categories(JSON: $0)!}
                            print(appDel.movieCategories.count)
                        }
                        self.registrationInstructionFlow()
                    } else {
                        self.alertOk(title: "", message: response!["msg"] as! String)
                    }
                } else {
                    self.alertOk(title: "", message: "Something went wrong please try again!")
                }
                WebService.Loader.hide()
            }
        }
    
    func registrationInstructionFlow()  {
        if signup_video_status == "1"{
            let vc = InstructionVideoVC.init(nibName: "InstructionVideoVC", bundle: .main)
            vc.video_url = self.signup_video
            if appDel.loggedInUserData?.new_user == "1"{
                vc.isHideSkipButton = true
            }else{
                vc.isHideSkipButton = false
            }
            vc.blockInstruction = {
                appDel.setupProfile()
                appDel.displayScreen()
            }
            self.present(vc, animated: true, completion: nil)
        }else{
            appDel.setupProfile()
            appDel.displayScreen()
        }
    }
    
    //MARK:- Touch ID
    func chekTouchId() -> Bool {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
        var authError: NSError?
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            if #available(iOS 11.0, *) {
                switch (localAuthenticationContext.biometryType){
                case .none:
                    return false
                case .touchID:
                    return true
                case .faceID:
                    return true
                default: break
                }
            } else {
                // Fallback on earlier versions
                return false
            }
        } else {
            return false
        }
        return false
    }
}

//MARK:- API OTP Generate
extension MobileVerifyVC{
    func APICallgenerateOTP(_ mobile:String,code:String){
        let params = ["mobile_no": mobile,"country_code": code] as [String : Any]
        WebService.Request.patch(url: generateOTP, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    //
                    let objTab = OTPVerifyVC(nibName: "OTPVerifyVC", bundle: nil)
                    objTab.isFrom = .mobileScreen
                    objTab.otp = response!["otp"] as? String ?? ""
                    self.view.endEditing(true)
                    objTab.blockVerifyOtp = { isVerify in
                        if isVerify{
                            self.alertTwoButton(title: "Biometric Authentication", titleButtonAccept: "OK", titleButtonReject: "CANCEL", message: "Do you want to enableBiometric Authenticate") { (status) in
                                if status{
                                    if self.chekTouchId(){
                                        let objTab = BiometricAuthVC(nibName: "BiometricAuthVC", bundle: nil)
                                        objTab.isFrom = .mobileVerify
                                        objTab.authSuccess = {
                                           self.isCheckPassword = true
                                           self.apiCallInstruction()
                                        }
                                        self.present(objTab, animated: true, completion: nil)
                                    }else{
                                        self.isCheckPassword = true
                                        self.apiCallInstruction()
                                    }
                                }else{
                                    self.isCheckPassword = true
                                    self.apiCallInstruction()
                                }
                            }
                        }
                    }
                    self.navigationController!.pushViewController(objTab, animated: true)
                    //
                    
                    
//                    if response!["complete_month"] as? String == "1"{
//                       print("ok")
//                        self.apiCallForLoginRegister(self.txtMobile.text ?? "", code: self.lblCountryCode.text ?? Locale.current.regionCode ?? "")
//                    }else{
//                        print("No")
//                        let objTab = OTPVerifyVC(nibName: "OTPVerifyVC", bundle: nil)
//                        objTab.isFrom = .mobileScreen
//                        objTab.otp = response!["otp"] as? String ?? ""
//                        self.view.endEditing(true)
//                        objTab.blockVerifyOtp = { isVerify in
//                            if isVerify{
//                                self.apiCallForLoginRegister(self.txtMobile.text ?? "", code: self.lblCountryCode.text ?? Locale.current.regionCode ?? "")
//                            }
//                        }
//                        self.navigationController!.pushViewController(objTab, animated: true)
//                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}
