//
//  LanguagePopupVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class LanguagePopupVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var tblLanguage: UITableView!
    var arrLanguages = [String]()
    var selectedLanguage: ((NSInteger) -> ())?
    var Selectedindex = Int()
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        arrLanguages.insert("Default", at: 0)
        self.tblLanguage.register(CellLanguageOption.self)
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnClosePopupTapped(_ sender: UIButton) {
        selectedLanguage?(self.Selectedindex)
        self.dismiss(animated: true, completion: nil)
    }
}

extension LanguagePopupVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrLanguages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CellLanguageOption = tableView.dequeueReusableCellTb(for: indexPath)
        cell.lblTitle.text = self.arrLanguages[indexPath.item]
        
        if self.Selectedindex == indexPath.row
        {
            cell.imgTick.image = #imageLiteral(resourceName: "verified")
        }
        else
        {
            cell.imgTick.image = #imageLiteral(resourceName: "Radio")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.Selectedindex = indexPath.row
        selectedLanguage?(self.Selectedindex)
        self.tblLanguage.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
}

