//
//  CellLanguageOption.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class CellLanguageOption: UITableViewCell, ReusableView, NibLoadableView {

    //MARK:- Variables & Outlets
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
