//
//  MovieCastTblCell.swift
//  CineStool
//
//  Created by YB on 26/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class MovieCastTblCell: UITableViewCell,NibLoadableView, ReusableView {
    
    @IBOutlet weak var lblcastType: UILabel!
    @IBOutlet weak var lblcastName: UILabel!
    @IBOutlet weak var lblNoInfo: UILabel!
    @IBOutlet weak var stackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
