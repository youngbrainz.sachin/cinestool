import UIKit
import Cosmos
import AVKit
import AVFoundation
import GoogleCast

class MovieDetailVC: UIViewController {

    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var imgMovieThumb: UIImageView!
    @IBOutlet weak var viewCorner: UIView!
    @IBOutlet weak var viewCornerscrollViewBottom: UIView!
    @IBOutlet weak var viewCornerBottom: UIView!
    @IBOutlet weak var lblMovieDetail: UILabel!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var lblMovieReleaseDate: UILabel!
    @IBOutlet weak var btnTotalRating: UIButton!
    @IBOutlet weak var lblTotalRatingCount: UILabel!
    @IBOutlet weak var lblMyRating: UILabel!
    @IBOutlet weak var lblTotalWatched: UILabel!
    @IBOutlet weak var btnOverview: UIButton!
    @IBOutlet weak var lblOverviewLine: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var lblDetailLine: UILabel!
    @IBOutlet weak var btnGiveRating: UIButton!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var bottomConstraintScrollView: NSLayoutConstraint!
    @IBOutlet weak var topImageBanner: NSLayoutConstraint!
    @IBOutlet weak var heightRating: NSLayoutConstraint!
    @IBOutlet weak var widthRating: NSLayoutConstraint!
    @IBOutlet weak var heightmoviedetail: NSLayoutConstraint!
    @IBOutlet weak var heightName: NSLayoutConstraint!
    @IBOutlet weak var bottomViewMovieDetail: NSLayoutConstraint!//100
    @IBOutlet weak var btnPlayTrailer: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnSubscribe: UIButton!
    @IBOutlet weak var tblCast: UITableView!
    @IBOutlet weak var imgMyRateStar: UIImageView!
    @IBOutlet weak var horizontalBtnPlay: NSLayoutConstraint!
    
    var movieID = ""
    var movieDetail: Movies!
    var movieOverview = ""
    var movieDetails = ""
    var givenRating = ""
    var playerController = LandscapeAVPlayerController()
    var player = AVPlayer()
    var playerItem: AVPlayerItem!
    var timeObserver: Any?
    var isDoResume = false
    let timeForAdds = 900
    var tempSec = 900
    var isMultipleAudio = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onloadController()
    }
    
    func onloadController()  {
        self.horizontalBtnPlay.constant = 0
        tblCast.register(MovieCastTblCell.self)

        widthRating.constant = UIScreen.main.bounds.width - 50
        if Double((widthRating.constant - 40) / 10) > 50 {
            ratingBar.settings.starSize = 50.0
            widthRating.constant = 54 * 10

        } else {
            ratingBar.settings.starSize = Double((widthRating.constant - 40) / 10)
        }
        ratingBar.settings.starMargin = 4.0
        ratingBar.didFinishTouchingCosmos = { rating in
            print(rating)
            self.givenRating = "\(rating)"
            self.lblMyRating.text = "\(Int(rating))"
        }
        
        heightmoviedetail.constant = 325 + heightName.constant + (lblMovieDetail.text?.height(constraintedWidth: lblMovieDetail.frame.size.width, font: UIFont (name: "Poppins-Regular", size: 16.0)!))!
        self.view.updateConstraintsIfNeeded()
        ratingBar.isHidden = true
        btnDetail.alpha = 0.4
        btnOverview.alpha = 1.0
        lblOverviewLine.isHidden = false
        lblDetailLine.isHidden = true
        viewCornerBottom.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDel.myOrientation = .portrait
        appDel.isFullScreen = false
        apiCallGetMovieDetails()
        addBlurBackground(control: imgBlur)
        addCornerOnTopLeftTopRight(control: viewCorner)
        addCornerOnBottomLeftTopRight(control: viewCornerBottom)
        addCornerOnBottomLeftTopRight(control: viewCornerscrollViewBottom)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
    }
    
    func apiCallGetMovieDetails() {
        let params = ["movie_id":self.movieID,"mobile_uuid":appDel.getDeviceUUID()] as [String : Any]
        
        WebService.Request.patch(url: getMovieDetail, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String : Any] {
                        self.movieDetail = Movies(JSON: data)
                        self.imgBlur.sd_setImage(with: URL(string: self.movieDetail.poster_image), placeholderImage: #imageLiteral(resourceName: "HomeLogo"))
                        self.imgMovieThumb.sd_setImage(with: URL(string: self.movieDetail.movie_banner), placeholderImage: #imageLiteral(resourceName: "HomeLogo"))
                        self.lblMovieName.text = self.movieDetail.name
                        self.lblMovieReleaseDate.text = "\(self.movieDetail.month_name), " + self.movieDetail.year
                        self.lblMovieDetail.text = self.movieDetail.description
                        
                        if self.movieDetail.total_rating == "" || self.movieDetail.total_rating == "0.0" || self.movieDetail.total_rating == "0" {
                            self.btnTotalRating.setTitle("", for: .normal)
                        }else {
                            let rate:Float = Float("\(self.movieDetail.total_rating )") ?? 0.0
                            let doubleStr1 = String(format: "%.2f", rate)
                            self.btnTotalRating.setTitle("\(doubleStr1.dropLast())", for: .normal)
                        }
                        
                        if self.movieDetail.total_watching == "" || self.movieDetail.total_watching == "0" {
                            self.lblTotalWatched.text = "-"
                        }else{
                            let doubleValue = Double(self.movieDetail.total_watching)
//                            self.lblTotalWatched.text = Common.formatNumber(doubleValue ?? 0.00)
                            self.lblTotalWatched.text = self.movieDetail.total_watching
                        }
                        
                        if self.movieDetail.my_rating == "0.0" || self.movieDetail.my_rating == "0" || self.movieDetail.my_rating == "" {
                            self.btnGiveRating.isHidden = false
                        } else {
                            self.btnGiveRating.isHidden = true
                        }
                        
                        let intRating = (self.movieDetail.my_rating as NSString).integerValue
                        if intRating == 0 {
                            self.lblMyRating.text = ""
                            self.imgMyRateStar.image = #imageLiteral(resourceName: "starBlank")
                        } else {
                           self.lblMyRating.text = "\(intRating)"
                            self.imgMyRateStar.image = #imageLiteral(resourceName: "starFill")
                        }
                        if self.movieDetail.movie_trailer == "" {
                            self.btnPlayTrailer.isHidden = true
                            self.horizontalBtnPlay.constant = 0
                        } else {
                            self.btnPlayTrailer.isHidden = false
                            self.horizontalBtnPlay.constant = 72
                        }
                        
                        if self.movieDetail.is_subscribe == "" || self.movieDetail.is_subscribe == "0"{
                            self.btnNotification.setImage(#imageLiteral(resourceName: "notificationBell"), for: .normal)
                            self.btnSubscribe.setTitle("SUBSCRIBE", for: .normal)
                        } else {
                            self.btnNotification.setImage(#imageLiteral(resourceName: "notificationAll"), for: .normal)
                            self.btnSubscribe.setTitle("SUBSCRIBED", for: .normal)
                        }
                        let strSubscribed = self.movieDetail.is_plan_subscribed
                        if strSubscribed == "1"{
                            appDel.is_Subscribed = true
                        }else{
                            appDel.is_Subscribed = false
                        }
                        self.movieDetails = self.movieDetail.description
                        self.movieOverview = self.movieDetail.description
                        self.heightmoviedetail.constant = 325 + self.heightName.constant + (self.lblMovieDetail.text?.height(constraintedWidth: self.lblMovieDetail.frame.size.width, font: UIFont (name: "Poppins-Regular", size: 16.0)!))!
                        self.tblCast.reloadData()
                        self.view.updateConstraintsIfNeeded()
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallGiveRating() {
        let params = ["movie_id":self.movieID,
                      "rating": self.givenRating]
        
        WebService.Request.patch(url: giveMovieRating, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.btnGiveRating.isHidden = true
                    self.ratingBar.isHidden = true
                    self.bottomConstraintScrollView.constant = 64
                    self.bottomViewMovieDetail.constant = 100
                    self.apiCallGetMovieDetails()
                    self.alertOk(title: "", message: response!["msg"] as! String)
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    @IBAction func btnOverviewAction(_ sender: UIButton) {
        self.tblCast.isHidden = true
        btnDetail.alpha = 0.4
        btnOverview.alpha = 1.0
        lblOverviewLine.isHidden = false
        lblDetailLine.isHidden = true
        lblMovieDetail.text = movieOverview
        heightmoviedetail.constant = 325 + heightName.constant + (lblMovieDetail.text?.height(constraintedWidth: lblMovieDetail.frame.size.width, font: UIFont (name: "Poppins-Regular", size: 16.0)!))!
        self.view.updateConstraintsIfNeeded()
    }
    
    @IBAction func btnDetailAction(_ sender: UIButton) {
        self.tblCast.isHidden = false
        heightmoviedetail.constant = 0
        btnDetail.alpha = 1.0
        btnOverview.alpha = 0.4
        lblOverviewLine.isHidden = true
        lblDetailLine.isHidden = false
        lblMovieDetail.text = movieDetails
        heightmoviedetail.constant = 325 + heightName.constant + self.tblCast.contentSize.height
        self.view.updateConstraintsIfNeeded()
    }
    
    @IBAction func btnbackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNotificationAction(_ sender: UIButton) {
        if btnNotification.currentImage == #imageLiteral(resourceName: "notificationAll"){
            btnNotification.setImage(#imageLiteral(resourceName: "notificationBell"), for: .normal)
            self.btnSubscribe.setTitle("SUBSCRIBE", for: .normal)
            self.apiCallNotificationSubscribe(movieDetail.producer_id, isSubscribe: "0")
        }else{
            btnNotification.setImage(#imageLiteral(resourceName: "notificationAll"), for: .normal)
            self.btnSubscribe.setTitle("SUBSCRIBED", for: .normal)
            self.apiCallNotificationSubscribe(movieDetail.producer_id, isSubscribe: "1")
        }
    }
    
    @IBAction func btnTrailerAction(_ sender: UIButton) {
//        let url = movieDetail.movie_trailer//.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
//        playVideo(strVideoURL: url, isTrailer: true)
        redirectToMoviePlayer(openFrom: .Trailer)
    }
    
    @IBAction func btnPlayAction(_ sender: UIButton) {
        if movieDetail.final_video == "" ||  movieDetail.final_video.count == 0 {
            alertOk(title: "", message: StringText.videoNotAvail)
        }else if appDel.is_Subscribed && appDel.isAppLive == appStatus.Live{
            if self.movieDetail.is_watched == "0" || self.movieDetail.is_watched == "" {
                self.apiCallAddMovieViewCount()
            }
            redirectToMoviePlayer(openFrom: .Movie)
        }else if movieDetail.is_paid_movie == "Yes" && appDel.isAppLive == appStatus.Live{
            if appDel.isAppLive == appStatus.Live && !appDel.isPressedNoToSubscriptionPopup{
                self.alertTwoButton(title: "Subscription", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Do you want to enroll in cinestool monthly subscription?") { (result) in
                    UserDefaults.standard.set(false, forKey: UserdefaultsConstants.isJustLogin)
                    UserDefaults.standard.synchronize()
                    appDel.isJustLoggedIn = false
                if result{
                    let objPrivacyPolicyVC = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
                    findtopViewController()?.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                }else{
                        UserDefaults.standard.set(true, forKey: UserdefaultsConstants.isPressedNoToSubscriptionPopup)
                        appDel.isPressedNoToSubscriptionPopup = true
                        UserDefaults.standard.synchronize()
                    self.playMovieWithPayPerView()
                    }
                }
            }else{
                self.playMovieWithPayPerView()
            }
        }else if !appDel.is_Subscribed && appDel.isAppLive == appStatus.Live{
            if appDel.isAppLive == appStatus.Live && !appDel.isPressedNoToSubscriptionPopup{
                self.alertTwoButton(title: "Subscription", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Do you want to enroll in cinestool monthly subscription?") { (result) in
                    UserDefaults.standard.set(false, forKey: UserdefaultsConstants.isJustLogin)
                    UserDefaults.standard.synchronize()
                    appDel.isJustLoggedIn = false
                if result{
                    let objPrivacyPolicyVC = SubscriptionListVC(nibName: "SubscriptionListVC", bundle: nil)
                    findtopViewController()?.navigationController?.pushViewController(objPrivacyPolicyVC, animated: true)
                }else{
                        UserDefaults.standard.set(true, forKey: UserdefaultsConstants.isPressedNoToSubscriptionPopup)
                        appDel.isPressedNoToSubscriptionPopup = true
                        UserDefaults.standard.synchronize()
                        if self.movieDetail.is_watched == "0" || self.movieDetail.is_watched == "" {
                            self.apiCallAddMovieViewCount()
                        }
                        self.redirectToMoviePlayer(openFrom: .Movie)
                    }
                }
            }else{
                if self.movieDetail.is_watched == "0" || self.movieDetail.is_watched == "" {
                    self.apiCallAddMovieViewCount()
                }
                self.redirectToMoviePlayer(openFrom: .Movie)
            }
        } else {
            if self.movieDetail.is_watched == "0" || self.movieDetail.is_watched == "" {
                self.apiCallAddMovieViewCount()
            }
            redirectToMoviePlayer(openFrom: .Movie)
//            self.playVideo(strVideoURL: movieDetail.final_video, isTrailer: false)
        }
    }
    
    func playMovieWithPayPerView() {
        if self.movieDetail.is_movie_payment_done == "1"{
//                self.playVideo(strVideoURL: movieDetail.final_video, isTrailer: false)
            if self.movieDetail.is_watched == "0" || self.movieDetail.is_watched == "" {
                self.apiCallAddMovieViewCount()
            }
            self.redirectToMoviePlayer(openFrom: .Movie)
        }else{
            appDel.blockPaymentDone = {
                if self.movieDetail.is_watched == "0" || self.movieDetail.is_watched == "" {
                    self.apiCallAddMovieViewCount()
                }
                self.redirectToMoviePlayer(openFrom: .Movie)
//                    self.playVideo(strVideoURL: self.movieDetail.final_video, isTrailer: false)
            }
            let objTab2 = PaymentPopupVC(nibName: "PaymentPopupVC", bundle: .main)
            objTab2.modalPresentationStyle = .overFullScreen
            objTab2.price = self.movieDetail.movie_price
            objTab2.subscriptionPrice = self.movieDetail.movie_price
            objTab2.actual_price = self.movieDetail.movie_actual_price
            objTab2.actual_symbol = self.movieDetail.currency_actual_symbol
            objTab2.movieID = self.movieDetail._id
                objTab2.PopupFile = .PaidNow
            self.present(objTab2, animated: true, completion: nil)
        }
    }
    
    func playVideo(strVideoURL: String, isTrailer: Bool) {
        if isTrailer {
            let urlStr : String = strVideoURL
            let urls = URL(string: urlStr)
            playerController = LandscapeAVPlayerController()
            playerItem = AVPlayerItem (url: urls!)
            player = AVPlayer (playerItem: playerItem)
            playerController.player = player
            present(playerController, animated: true) { UIDevice.current.setValue(Int(UIInterfaceOrientationMask.landscape.rawValue), forKey: "orientation")
                self.playerController.player?.play()
            }
        } else {
            if movieDetail.is_watched == "0" || movieDetail.is_watched == "" {
                apiCallAddMovieViewCount()
            }
            playerController = LandscapeAVPlayerController()
            let urlStr : String = strVideoURL
                //.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let urls = URL(string: urlStr)
            playerItem = AVPlayerItem (url: urls!)
            player = AVPlayer (playerItem: playerItem)
            playerController.player = player
            tempSec = timeForAdds
            present(playerController, animated: true) {
                UIDevice.current.setValue(Int(UIInterfaceOrientationMask.landscape.rawValue), forKey: "orientation")
                self.playerController.player?.play()
            }
            self.displayLanguageAlert()
            let interval = CMTime(seconds: 0.01, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            self.timeObserver = self.player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { elapsedTime in
                
                self.updateVideoPlayerState()
            })
        }
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
    }
    
    func redirectToMoviePlayer(openFrom: redirectToMoviePlayerFrom){
        let storyboard = UIStoryboard(name: "MoviePlayer", bundle: nil)
        let moviePlayer = storyboard.instantiateViewController(withIdentifier: "MoviePlayerVC") as? MoviePlayerVC
        moviePlayer?.modalPresentationStyle = .overFullScreen
        moviePlayer?.movieDetail = movieDetail
        moviePlayer?.openFrom = openFrom
//        let castContainerVC =
//                GCKCastContext.sharedInstance().createCastContainerController(for: moviePlayer!)
//        castContainerVC.miniMediaControlsItemEnabled = true
        self.navigationController!.pushViewController(moviePlayer!, animated: true)
    }
    
    func displayLanguageAlert()  {
            let alert = UIAlertController (title: "Voice Over Language", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

            let fullString = NSMutableAttributedString (string: "Currently You\'re Watching this movie in it default language. To change it, click on the icon ")
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = #imageLiteral(resourceName: "lang")
            let image1String = NSAttributedString(attachment: image1Attachment)
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: " at the Bottom-Right corner on this screen. If the movie has voice over language available, you will see it and make a selection of your choice. Thank you."))

            alert.setValue(fullString, forKey: "attributedMessage")

            self.presentedViewController!.present(alert, animated: true, completion: nil)
    }
    
    func updateVideoPlayerState() {
        let currentTime = player.currentTime()
        let currentTimeInSeconds = CMTimeGetSeconds(currentTime)
        if player.currentItem != nil {
            let timeformatter = NumberFormatter()
            guard let sec = timeformatter.string(from: NSNumber(value: currentTimeInSeconds)) else {
                return
            }
//            if sec == "1" {
//                let alert = UIAlertController (title: "Voice Over Language", message: "", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//
//                let fullString = NSMutableAttributedString (string: "Currently You\'re Watching this movie in it default language. To change it, click on the icon ")
//                let image1Attachment = NSTextAttachment()
//                image1Attachment.image = #imageLiteral(resourceName: "lang")
//                let image1String = NSAttributedString(attachment: image1Attachment)
//                fullString.append(image1String)
//                fullString.append(NSAttributedString(string: " at the Bottom-Right corner on this screen. If the movie has voice over language available, you will see it and make a selection of your choice. Thank you."))
//
//                alert.setValue(fullString, forKey: "attributedMessage")
//
//                self.presentedViewController!.present(alert, animated: true, completion: nil)
//            }
            if sec == "\(tempSec)" {
                if movieDetail.is_advertisement_show == "1" &&  movieDetail.is_advertisement_payment_done == "0" && movieDetail.is_paid_movie == "No"{
                    self.player.pause()
                    tempSec += timeForAdds
                    let objTab = AdvertisementVC(nibName: "AdvertisementVC", bundle: .main)
                    objTab.movieID = self.movieID
                    objTab.blockAddClosed = { isUpdate in
                    UIDevice.current.setValue(Int(UIInterfaceOrientationMask.landscape.rawValue), forKey: "orientation")
                        self.movieDetail.is_advertisement_payment_done = isUpdate
                        self.player.play()
                    }
                    presentedViewController?.present(objTab, animated: true, completion: nil)
                    self.isDoResume = true
                }
            }
        }
    }
    
    @IBAction func btnGiveRatingAction(_ sender: UIButton) {
        viewCornerBottom.isHidden = false
        bottomViewMovieDetail.constant = 0
        ratingBar.isHidden = false
        bottomConstraintScrollView.constant = 270
        self.view.updateConstraintsIfNeeded()
        if self.givenRating != "" {
            apiCallGiveRating()
        }
    }
}

class LandscapeAVPlayerController: AVPlayerViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        self.player?.volume = 0.5
        if isInstruction{
            UIDevice.current.setValue(Int(UIInterfaceOrientationMask.all.rawValue), forKey: "orientation")
        }else{
            UIDevice.current.setValue(Int(UIInterfaceOrientationMask.landscape.rawValue), forKey: "orientation")
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.player?.pause()
        if (self.isMovingFromParent) {
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        if isInstruction{
            blockInstructionDone()
        }
    }
    
    var blockInstructionDone: (()->())!
    @objc func canRotate() -> Void {}
    var isInstruction = false
}

extension MovieDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.movieDetail != nil {
            if self.movieDetail.cast_data.count != 0 {
                return self.movieDetail.cast_data.count
            } else {
                return 1
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MovieCastTblCell = tableView.dequeueReusableCellTb(for: indexPath)
        if self.movieDetail.cast_data.count != 0 {
            cell.lblcastType.text = self.movieDetail.cast_data[indexPath.row].cast_type + " : "
            cell.lblcastName.text = self.movieDetail.cast_data[indexPath.row].cast_name
        } else {
            cell.stackView.isHidden = true
            cell.lblNoInfo.text = "No Information"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}

extension MovieDetailVC{
    func apiCallAddMovieViewCount() {
        let params = ["movie_id":self.movieID,"mobile_uuid":appDel.getDeviceUUID()] as [String : Any]
        WebService.Request.patch(url: addMovieViewCount, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallNotificationSubscribe(_ pId:String,isSubscribe:String) {
        let params = ["producer_id":pId,"is_subscribe":isSubscribe] as [String : Any]
        WebService.Request.patch(url: makeSubscribeChannel, type: .post, parameter: params) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
}

extension MovieDetailVC {
    @objc func playerDidFinishPlaying(note: NSNotification) {
        self.dismiss(animated: true) {
        }
    }
}
