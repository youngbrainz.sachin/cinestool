//
//  BiometricAuthVC.swift
//  CineStool
//
//  Created by YB on 16/12/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import LocalAuthentication
import GoogleCast
import KWDrawerController

class BiometricAuthVC: UIViewController {
    
    var isFrom: screenFrom = .none
    var authSuccess: (()->())!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        authenticationWithTouchID()
    }
    //MARK:- Touch ID
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
        var authError: NSError?
        localAuthenticationContext.localizedFallbackTitle = ""
        let reasonString = "Authenticate with Touch ID"
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            if #available(iOS 11.0, *) {
                switch (localAuthenticationContext.biometryType){
                case .none:
                    self.dismiss(animated: true, completion: {
                        self.authSuccess()
                    })
                case .touchID:
                    self.checkBiometric(localContect: localAuthenticationContext, resonString:reasonString)
                case .faceID:
                    self.checkBiometric(localContect: localAuthenticationContext, resonString:reasonString)
                default: break
                }
            } else {
                // Fallback on earlier versions
                self.dismiss(animated: true, completion: {
                    self.authSuccess()
                })
            }
        } else {
            guard let error = authError else {
                return
            }
            self.dismiss(animated: true, completion: {
                self.authSuccess()
            })
            print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
        }
    }
    
    func checkBiometric(localContect: LAContext, resonString: String) {
        localContect.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: resonString) { success, evaluateError in
            if success {
                DispatchQueue.main.async {
                    if self.isFrom == .mobileVerify {
                        UserDefaults.standard.set(true, forKey: "isTouchID")
                        self.dismiss(animated: true, completion: {
                            self.authSuccess()
                        })
                    } else {
                        self.displayScreen()
                    }
                }
            } else {
                guard let error = evaluateError else {
                    return
                }
                print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
            }
        }
    }
    
    func displayScreen()  {
        if appDel.loginUser_Id != "" {
            if appDel.loggedInUserData?.new_user == "1" {
                let viewController = CreateProfileVC(nibName: "CreateProfileVC", bundle: .main)
                viewController.isFrom = .mobileVerify
                viewController.mobileNumber = appDel.loggedInUserData?.mobile_no ?? ""
                viewController.mobileCode = appDel.loggedInUserData?.country_code ?? ""
                self.navigationController?.pushViewController(viewController, animated: true)
            }else{
                let mainViewController = MainContainerVC(nibName: "MainContainerVC", bundle: .main)
                let menuViewController = SideMenuVC(nibName: "SideMenuVC", bundle: .main)
                //Set for callback use
                appDel.sideMenuVC = menuViewController
                
                let aDrawer = DrawerController()
                aDrawer.setViewController(mainViewController, for: .none)
                aDrawer.setViewController(menuViewController, for: .left)
                
                aDrawer.drawerWidth = Float((UIScreen.main.bounds.size.width / 3) * 2) + 30
//                let castContainerVC =
//                        GCKCastContext.sharedInstance().createCastContainerController(for: aDrawer)
//                castContainerVC.miniMediaControlsItemEnabled = true

                self.navigationController?.pushViewController(aDrawer, animated: true)
            }
        } else {
            let viewController = MobileVerifyVC(nibName: "MobileVerifyVC", bundle: .main)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }

    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                
            default:
                message = "Did not find error code on LAError object"
            }
        }
        return message;
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            authenticationWithTouchID()
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            authenticationWithTouchID()
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        return message
    }
}

