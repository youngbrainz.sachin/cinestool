//
//  IPayVC.swift
//  CineStool
//
//  Created by YB on 25/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class IPayVC: UIViewController,WKNavigationDelegate,WKUIDelegate {

    //MARK:- Variables & Outlets
    @IBOutlet weak var viewWeb: UIView!
    
    var mywebView: WKWebView!
    var load_url = ""
    var success_url = String()
    var cancelled_url = String()
    var unique_token = String()
    var error_url = String()
    var blockcardAdded: ((Bool)->())!
    var paymentType = String()
    var paymentFor = String()
    var amount = String()
    var movieID = String()
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let webConfig = WKWebViewConfiguration()
        mywebView = WKWebView (frame: .zero, configuration: webConfig)
        mywebView.uiDelegate = self
        viewWeb = mywebView
        mywebView.navigationDelegate = self
        let urlStr : String = self.load_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url: URL = URL(string: urlStr)!
        mywebView.load(URLRequest(url: url))
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnBackTapped(_ sender: UIButton) {
        presentingViewController!.dismiss(animated: true)
    }
}

extension IPayVC: UIWebViewDelegate
{
    //MARK:- Webview Delegate
    internal func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        return true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let text = webView.url?.absoluteString {
            print(text)
            
            if text.contains("Cancelled") {
                let alert = UIAlertController(title: "", message: "The user cancelled payment.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (Action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else if text.contains("Error") {
                let alert = UIAlertController(title: "", message: "Something went wrong.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (Action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else if text.contains("Success") {
                self.Payment_Success()
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        let alert = UIAlertController(title: "", message: "The user canceled payment.", preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (Action) in
//            self.dismiss(animated: true, completion: nil)
//        }))
//        self.present(alert, animated: true, completion: nil)
    }
    
}

extension IPayVC
{
    func Payment_Success() {
        var param = ["user_id": appDel.loginUser_Id,
                     "payment_type" : "3",
                     "amount" : amount,
                     "movie_id": movieID,
                     "payment_for": paymentFor,"mobile_uuid":appDel.getDeviceUUID()]
        param["user_type"] = appDel.userType
        param["loginuser_id"] = appDel.loginUser_Id
        param["session_token"] = appDel.userSessionToken
        self.startAnimatingLoader(message: "")
        Alamofire.request(makePayment, method: .post , parameters: param).validate().responseJSON { response in
            self.stopAnimatingLoader()
                switch response.result {
                case .success:
                    var jsonResult = [String:AnyObject]()
                    do
                    {
                        jsonResult = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String:AnyObject]
//                        print(jsonResult)
//                        if jsonResult["success"] as! Int == 1 {
                            self.alertOk(title: "", message: "Your Payment has been succesfully done.", buttonTitle: "OK", completion: { (Bool) in
                            self.blockcardAdded(true)
                           self.dismiss(animated: true, completion: nil)
                        })
//                    }
                }
                catch let error as NSError {
                    print("--->",error)
                }
            case .failure(let error):
                print("---->",error)
            }
        }
    }
}
