//
//  PaymentVC.swift
//  CineStool
//
//  Created by YB on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import IQKeyboardManagerSwift
import AVKit


class PaymentVC: UIViewController {
    
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var imgCardChecked: UIImageView!
    @IBOutlet weak var imgPayPalChecked: UIImageView!
    @IBOutlet weak var imgMobileMoneyChecked: UIImageView!
    
    @IBOutlet weak var txtCardName: ACFloatingTextfield!
    @IBOutlet weak var txtCardNumber: ACFloatingTextfield!
    @IBOutlet weak var txtCardExpiry: ACFloatingTextfield!
    @IBOutlet weak var txtCardCVV: ACFloatingTextfield!
    @IBOutlet weak var heightCard: NSLayoutConstraint!
    
    @IBOutlet weak var viewDatePick: UIView!
    @IBOutlet weak var viewMonthYearPicker: MonthYearPickerView!
    
    @IBOutlet weak var viewMobileMoneyInfo: UIView!
    @IBOutlet weak var lblPopUpPrice: UILabel!
    
    var selectedmonth = String()
    var selectedyear = String()
    var selectedname = String()
    var selectedCardNumber = String()
    var selectedDate = String()
    var selectedCVV = String()
    var selectedMonth = String()
    var selectedYear = String()
    var isFrom: screenFrom = .none
    var paymentType = "2"
    var paymentFor = ""
    var subscriptionPrice = String()
    var movieID = ""
    var load_url = String()
    var success_url = String()
    var cancelled_url = String()
    var unique_token = String()
    var error_url = String()
    var webview_url = String()
    var product_charge_ghana = String()
    var price = ""
    var actual_price = ""
    var actual_symbol = ""

    var blockPaymentDone: (()->())!
    var isCardAvail = false
    
    var mobile_money_video = ""
    var signup_video = ""
    var str_instruction = ""
    var signup_video_status = "0"
    var mobile_money_video_status = "0"
    var mob_status_text = "0"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        onloadOperation()
    }
    
    func onloadOperation() {
        hideView()
        hideShowViews(isHide: false, height: 210, imgCard: #imageLiteral(resourceName: "RadioDisable"), imgPayPal: #imageLiteral(resourceName: "RadioUnable"), imgMobileMoney: #imageLiteral(resourceName: "RadioUnable"))
        IQKeyboardManager.shared.enable = true
        if isFrom == .advertisement {
           paymentFor = "1"
        } else {
           paymentFor = "2"
        }
        
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        actual_symbol = currencyCode ?? "$"
        lblPopUpPrice.text = "\(actual_symbol)\(price)"
        get_urlIPAy()
        apiCallGetCard()
    }
    
    func hideShowViews(isHide: Bool, height: CGFloat, imgCard: UIImage, imgPayPal: UIImage, imgMobileMoney: UIImage) {
        UIView.animate(withDuration: 0.4) {
            self.imgCardChecked.image = imgCard
            self.imgPayPalChecked.image = imgPayPal
            self.imgMobileMoneyChecked.image = imgMobileMoney
            self.heightCard.constant = height
            self.viewCard.isHidden = isHide
            self.view.layoutIfNeeded()
        }
    }
    
    func hideView() {
        viewDatePick.isHidden = true
    }
    
    func ShowView() {
        viewDatePick.isHidden = false
    }
    
    func get_urlIPAy(){
        var param = [String:Any]()
        param = ["amount": price,
                 "currency_code": appDel.loggedInUserData?.currency_code ?? "USD"]
            WebService.Request.patch(url: getPaymentURL, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.unique_token = response!["unique_token"] as? String ?? ""
                    self.webview_url = response!["webview_url"] as? String ?? ""
                    self.success_url = response!["success_url"] as? String ?? ""
                    self.cancelled_url = response!["cancelled_url"] as? String ?? ""
                    self.error_url = response!["error_url"] as? String ?? ""
                    self.product_charge_ghana = response!["product_charge_ghana"] as? String ?? ""
                } else {
                    
                }
            }
            else {
                
            }
        }
    }
    
    func apiCallMakePayment(amount: String, PaypalTransId: String) {
        
        var param = [String:Any]()
        param = ["payment_type" : paymentType,
                 "amount" : amount,
                 "movie_id": movieID,
                 "payment_for": paymentFor,"mobile_uuid":appDel.getDeviceUUID()]
        if paymentType == "1" {
            param["transactions_id"] = PaypalTransId
        } else if paymentType == "2" {
            param["card_number"] = selectedCardNumber
            param["cvv"] = selectedCVV
            param["expiry_month"] = txtCardExpiry.text!.components(separatedBy: "/").first
            param["expiry_year"] = txtCardExpiry.text!.components(separatedBy: "/").last
        } else {
            
        }
        WebService.Request.patch(url: makePayment, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    self.alertOk(title: "", message: response!["msg"] as! String, buttonTitle: "OK", completion: { (Bool) in self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: {
                            appDel.blockPaymentDone()
                        })
                    })
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func viewPayWithDisplay(status:Bool){
//        btnBackBlur.isHidden = status
//        viewPayWith.isHidden = status
    }
    
    @IBAction func btnCardAction(_ sender: UIButton) {
        hideShowViews(isHide: false, height: 210, imgCard: #imageLiteral(resourceName: "RadioDisable"), imgPayPal: #imageLiteral(resourceName: "RadioUnable"), imgMobileMoney: #imageLiteral(resourceName: "RadioUnable"))
        paymentType = "2"
    }
    
    @IBAction func btnPayPalAction(_ sender: UIButton) {
        hideShowViews(isHide: true, height: 0, imgCard: #imageLiteral(resourceName: "RadioUnable"), imgPayPal: #imageLiteral(resourceName: "RadioDisable"), imgMobileMoney: #imageLiteral(resourceName: "RadioUnable"))
        paymentType = "1"
    }
    
    @IBAction func btnMobileMoneyAction(_ sender: UIButton) {
        hideShowViews(isHide: true, height: 0, imgCard: #imageLiteral(resourceName: "RadioUnable"), imgPayPal: #imageLiteral(resourceName: "RadioUnable"), imgMobileMoney: #imageLiteral(resourceName: "RadioDisable"))
        paymentType = "3"
    }
    
    @IBAction func btnShowDatePicker(_ sender: UIButton) {
        self.view.endEditing(true)
        ShowView()
    }
    
    @IBAction func btnHideDatePicker(_ sender: UIButton) {
        let string = String(format: "%02d/%d", viewMonthYearPicker.month, viewMonthYearPicker.year)
        self.selectedmonth = String(format: "%02d", viewMonthYearPicker.month)
        self.selectedyear = String(format: "%d", viewMonthYearPicker.year)
        
        let date1Format = DateFormatter()
        date1Format.dateFormat = "MM/yyyy"
        let date1 = date1Format.date(from: string)
        
        let now = Date()
        let currDate = date1Format.string(from: now)
        let date2 = date1Format.date(from: currDate)
        
        if date1! < date2! {
            alertOk(title: "Cinestool", message: "Invalid Expiry Date")
            self.txtCardExpiry.text = ""
        }else{
            self.txtCardExpiry.text = string
        }
        
        hideView()
    }
    
    @IBAction func btnPayAction(_ sender: UIButton) {
        if paymentType == "2" {
            if validate() {
                if isCardAvail == false{
                    self.apiCallAddEditCard()
                }
                self.apiCallMakePayment(amount: actual_price, PaypalTransId: "")
            }
        } else if paymentType == "1" {
            //
//                PaypalIntegration.sharedClient.initPaypal()
//                PaypalIntegration.sharedClient.blockPaymentStatus = { (success,data) in
//                    if success {
//                        print("payment Success")
//                        print(data)
//                        self.apiCallMakePayment(amount: self.actual_price, PaypalTransId: (data["id"] as? String)!)
//                    } else {
//                        print("payment Failed")
//                    }
//                }
//                PaypalIntegration.sharedClient.configurePaypal(strMarchantName: "Name",acceptCreditCard: false, setenvironment: PayPalEnvironmentProduction)
//                PaypalIntegration.sharedClient.setItems(strItemName: "Wallet", noOfItem: "1", strPrice: self.actual_price, strCurrency: "USD", strSku: nil)
//            PaypalIntegration.sharedClient.goforPayNow(shipPrice: "0.0", taxPrice: "0.0", totalAmount: self.actual_price, strShortDesc: "CineStool Purchase", strCurrency: "USD",controller: self)
            
            //
        } else if paymentType == "3" {
            self.viewMobileMoneyInfo.isHidden = false
//            self.apiCallInstruction()
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {    
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnViewMobileMoneyHideAction(_ sender: UIButton) {
        self.viewMobileMoneyInfo.isHidden = true
    }
}

extension PaymentVC: UITextFieldDelegate{
    //Validate Fields
    func validate() -> Bool
    {
            if txtCardName.text == "" {
                alertOk(title: "", message: InsertCardName) { (result) in
                }
                return false
            }
            else if txtCardNumber.text == "" {
                alertOk(title: "", message: InsertCardNumber) { (result) in
                }
                return false
            }
            else if txtCardExpiry.text == "" {
                alertOk(title: "", message: InsertCardExpiryDate) { (result) in
                }
                return false
            }
            else if txtCardCVV.text == "" {
                alertOk(title: "", message: InsertCVV) { (result) in
                }
                return false
            }
            selectedname = txtCardName.text ?? ""
            selectedCardNumber = txtCardNumber.text ?? ""
            selectedMonth = selectedmonth
            selectedYear = selectedyear
            selectedCVV = txtCardCVV.text ?? ""
            self.view.endEditing(true)
            return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCardNumber || textField == txtCardNumber {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else if textField == txtCardCVV {
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}


extension PaymentVC{
    func apiCallAddEditCard() {
        var param = [String:Any]()
        param["holder_name"] = self.txtCardName.text
        param["card_number"] = self.txtCardNumber.text
        param["expiry_date"] = self.txtCardExpiry.text
        WebService.Request.patch(url: addUserCard, type: .post, parameter: param) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                }
            }
        }
    }
    
    func apiCallGetCard() {
        WebService.Request.patch(url: getUserCard, type: .post, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [[String: Any]] {
                        self.isCardAvail = true
                        let cardListing = data.map{cardListingModel(JSON: $0)!}
                        if cardListing.count > 0{
                            self.txtCardName.text = cardListing[0].holder_name
                            self.txtCardNumber.text = cardListing[0].card_number
                            self.txtCardExpiry.text = cardListing[0].expiry_date
                        }
                    }
                } else {
                    self.isCardAvail = false
                    //self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.isCardAvail = false
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func apiCallInstruction() {
        WebService.Request.patch(url: getCustomerInstruction, type: .get, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String: Any] {
                        self.mobile_money_video = data["mobile_money_video"] as! String
                        self.signup_video = data["signup_video"] as! String
                        self.str_instruction = data["mob_description"] as! String
                        self.signup_video_status = data["signup_video_status"] as! String
                        self.mobile_money_video_status = data["mobile_money_video_status"] as! String
                        self.mob_status_text = data["mob_status"] as! String
                    }
                }
            }
            self.paymetnInstructionFlow()
        }
    }
    
    func paymetnInstructionFlow()  {
        if mobile_money_video_status == "1"{
            let vc = InstructionVideoVC.init(nibName: "InstructionVideoVC", bundle: .main)
            vc.video_url = self.mobile_money_video
            vc.blockInstruction =
                {
                    if self.mob_status_text == "1"{
                        let vc = InstructionVC.init(nibName: "InstructionVC", bundle: .main)
                        vc.str_instruction = self.str_instruction
                        vc.blockInstructionDone = {
                            
                        }
                        self.present(vc, animated: true, completion: nil)
                    }else{
                        self.displayPaymentVC()
                    }
            }
            self.present(vc, animated: true, completion: nil)
        }else if mob_status_text == "1"{
            let vc = InstructionVC.init(nibName: "InstructionVC", bundle: .main)
            vc.str_instruction = self.str_instruction
            vc.blockInstructionDone = {
                self.displayPaymentVC()
            }
            self.present(vc, animated: true, completion: nil)
        }else{
            displayPaymentVC()
        }
    }
    
    func displayPaymentVC() {
        self.load_url = self.webview_url  + "?full_name=" + "\(appDel.loggedInUserData?.user_name ?? "")" + "&description=" + "CineStool" + "&email=" + "\(appDel.loggedInUserData?.email_id ?? "")" + "&amount=" + self.product_charge_ghana + "&unique_token=" + "\(self.unique_token)" + "&user_id=" + "\(appDel.loggedInUserData?._id ?? (appDel.loggedInUserData?.user_id ?? ""))" + "&movieID=" + "\(self.movieID)";
        let paymentVC = IPayVC.init(nibName: "IPayVC", bundle: .main)
        self.viewPayWithDisplay(status: true)
        paymentVC.unique_token = self.unique_token
        paymentVC.load_url = self.load_url
        paymentVC.success_url = self.success_url
        paymentVC.cancelled_url = self.cancelled_url
        paymentVC.error_url = self.error_url
        paymentVC.amount = self.actual_price
        paymentVC.movieID = self.movieID
        paymentVC.paymentType = self.paymentType
        paymentVC.paymentFor = self.paymentFor
        paymentVC.blockcardAdded = { paymentstatus in
            if paymentstatus {
                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: {
                    appDel.blockPaymentDone()
                })
            }
        }
        self.present(paymentVC, animated: true, completion: nil)
    }
}
