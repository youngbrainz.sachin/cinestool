//
//  VideoPlayerVC.swift
//  CineStool
//
//  Created by MacBook Air 002 on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import AVFoundation


class VideoPlayerVC: UIViewController {

    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var settings: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var rewindButton: UIButton!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    let videoPlayerView = UIView()
    var player: AVPlayer?
    var timeObserver: Any?
    var videoLanguage = [String]()
    var timer: Timer?
    var strVideoURL = "http://ybtestserver.in/netflix_html/images/video/video1.mp4"
    private var playerItemContext = 0
    var Selectedindex = 0
    var isDoResume = false
    var tempSec = 50
    var movieDetail: Movies!
    
    override func viewDidLoad() {
//        strVideoURL = "http://3.15.24.56/final.mp4"
        super.viewDidLoad()
        actInd.hidesWhenStopped = true
        actInd.style = UIActivityIndicatorView.Style.whiteLarge
        actInd.color = .red
        self.view.bringSubviewToFront(actInd)
        actInd.startAnimating()
        progressSlider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        onLoadController()
    }
    
    func onLoadController()  {
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        videoPlayerView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: videoPlayerView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: videoPlayerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint = NSLayoutConstraint(item: videoPlayerView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: videoPlayerView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
        
        view.addSubview(videoPlayerView)
        view.addConstraints([topConstraint, bottomConstraint, leadingConstraint, trailingConstraint])
        view.sendSubviewToBack(videoPlayerView)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(toggleControls))
        view.addGestureRecognizer(tapGesture)
        hideControls()
        backButton.isHidden = false
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        guard let duration = self.player?.currentItem?.duration else { return }
        let value = Float64(self.progressSlider.value) * CMTimeGetSeconds(duration)
        let seekTime = CMTime(value: CMTimeValue(value), timescale: 1)
        self.player?.seek(to: seekTime )
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeLeft
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.player?.pause()
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isDoResume {
            setupVideoPlayer(strVideoUrl: strVideoURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, selectedLanguageInadex: 0)
            resetTimer()
        } else {
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    @objc func toggleControls() {
        settings.isHidden = !settings.isHidden
        backButton.isHidden = !backButton.isHidden
        playPauseButton.isHidden = !playPauseButton.isHidden
        progressSlider.isHidden = !progressSlider.isHidden
        timeRemainingLabel.isHidden = !timeRemainingLabel.isHidden
        forwardButton.isHidden = !forwardButton.isHidden
        rewindButton.isHidden = !rewindButton.isHidden
        titleLabel.isHidden = !titleLabel.isHidden
        resetTimer()
    }
    
    @IBAction func playbackSliderValueChanged(_ sender:UISlider)
    {
        DispatchQueue.main.async { [weak self] in
            guard let duration = self!.player?.currentItem?.duration else { return }
            let value = Float64(self!.progressSlider.value) * CMTimeGetSeconds(duration)
            let seekTime = CMTime(value: CMTimeValue(value), timescale: 1)
            self!.player?.seek(to: seekTime )
        }
    }
    
    @IBAction func btnSettings(_ sender: UIButton) {
        player?.pause()
        let objLanguagePopupVC = LanguagePopupVC(nibName: "LanguagePopupVC", bundle: nil)
        objLanguagePopupVC.modalPresentationStyle = .overFullScreen
        objLanguagePopupVC.arrLanguages = videoLanguage
        objLanguagePopupVC.Selectedindex = Selectedindex
        objLanguagePopupVC.selectedLanguage = { index in
            if self.Selectedindex == index{
                self.player?.play()
            }else{
                self.Selectedindex = index
                DispatchQueue.main.async { [weak self] in
                    self?.changeLanguage(strVideoUrl: self!.strVideoURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, selectedLanguageInadex: self!.Selectedindex - 1)
                }
            }
        }
        self.present(objLanguagePopupVC, animated: true, completion: nil)
    }
    
    @IBAction func btnPlayVideo(_ sender: UIButton) {
        guard let player = player else { return }
        if !player.isPlaying {
            player.play()
            playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
        } else {
            playPauseButton.setImage(UIImage(named: "playvideo"), for: .normal)
            player.pause()
        }
    }
    
    @IBAction func jumpForward(_ sender: UIButton) {
        guard let currentTime = player?.currentTime() else { return }
        let currentTimeInSecondsPlus10 =  CMTimeGetSeconds(currentTime).advanced(by: 10)
        let seekTime = CMTime(value: CMTimeValue(currentTimeInSecondsPlus10), timescale: 1)
        player?.seek(to: seekTime)
    }
    
    @IBAction func jumpBackward(_ sender: UIButton) {
        guard let currentTime = player?.currentTime() else { return }
        let currentTimeInSecondsMinus10 =  CMTimeGetSeconds(currentTime).advanced(by: -10)
        let seekTime = CMTime(value: CMTimeValue(currentTimeInSecondsMinus10), timescale: 1)
        player?.seek(to: seekTime)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
}

//MARK:- Setup Video Player
extension VideoPlayerVC{
    func setupVideoPlayer(strVideoUrl: String, selectedLanguageInadex: NSInteger) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else {
                return
            }
            
            // 2
            DispatchQueue.main.async { [weak self] in
                // 3
                guard let url = URL(string: strVideoUrl) else {
                    return
                }
                let item = AVPlayerItem(url: url)
                NotificationCenter.default.addObserver(self!, selector: #selector(self?.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
                item.addObserver(self!,
                                       forKeyPath: #keyPath(AVPlayerItem.status),
                                       options: [.old, .new],
                                       context: &self!.playerItemContext)
                item.addObserver(self!, forKeyPath: "playbackBufferEmpty", options: .new, context: &self!.playerItemContext)
                item.addObserver(self!, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: &self!.playerItemContext)
                item.addObserver(self!, forKeyPath: "playbackBufferFull", options: .new, context: &self!.playerItemContext)

                self?.videoLanguage = item.tracks(type: .audio)
                self?.player = AVPlayer(playerItem: item)
                
                let playerLayer = AVPlayerLayer(player: self?.player)
                playerLayer.frame = (self?.videoPlayerView.bounds)!;
                self?.videoPlayerView.layer.addSublayer(playerLayer)
                self?.player?.play()
                let interval = CMTime(seconds: 0.01, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
                self?.timeObserver = self?.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { elapsedTime in
                    self!.updateVideoPlayerState()
                })
            }
        }
    }
    
    func changeLanguage(strVideoUrl: String, selectedLanguageInadex: NSInteger)  {
        self.actInd.startAnimating()
        player?.pause()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else {
                return
            }
            
            // 2
            DispatchQueue.main.async { [weak self] in
                guard let currentTime = self?.player?.currentTime() else { return }
                let currentTimeInSecondsPlus10 =  CMTimeGetSeconds(currentTime).advanced(by: 0)
                let seekTime = CMTime(value: CMTimeValue(currentTimeInSecondsPlus10), timescale: 1)
                
                guard let url = URL(string: strVideoUrl) else {
                    return
                }
                let item = AVPlayerItem(url: url)
                NotificationCenter.default.addObserver(self!, selector: #selector(self?.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
                item.addObserver(self!,
                                 forKeyPath: #keyPath(AVPlayerItem.status),
                                 options: [.old, .new],
                                 context: &self!.playerItemContext)
                item.addObserver(self!, forKeyPath: "playbackBufferEmpty", options: .new, context: &self!.playerItemContext)
                item.addObserver(self!, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: &self!.playerItemContext)
                item.addObserver(self!, forKeyPath: "playbackBufferFull", options: .new, context: &self!.playerItemContext)
                //Get list of audios.
                if (self?.videoLanguage.count)! > selectedLanguageInadex && self!.Selectedindex > 0{
                    let audioSelectionGroup = item.asset.mediaSelectionGroup(forMediaCharacteristic: .audible)
                    let option = audioSelectionGroup?.options
                    //            audioSelectionGroup?.mediaSelectionOption(withPropertyList: option![1])
                    item.select(option![selectedLanguageInadex], in: audioSelectionGroup!)
                }
                self?.player = AVPlayer(playerItem: item)
                
                let playerLayer = AVPlayerLayer(player: self?.player)
                playerLayer.frame = (self?.videoPlayerView.bounds)!;
                self?.videoPlayerView.layer.addSublayer(playerLayer)
                self?.player?.seek(to: seekTime)
                
                self?.player?.play()
                let interval = CMTime(seconds: 0.01, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
                self?.timeObserver = self?.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { elapsedTime in
                    self?.updateVideoPlayerState()
                })
            }
        }
    }
    
    @objc func playerDidFinishPlaying(sender: Notification) {
        // Your code here
        playPauseButton.setImage(UIImage(named: "playvideo"), for: .normal)
    }
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        
        // Only handle observations for the playerItemContext
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath,
                               of: object,
                               change: change,
                               context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            
            // Switch over status value
            switch status {
            case .readyToPlay:
                playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
                self.actInd.stopAnimating()
                break
            // Player item is ready to play.
            case .failed:
                self.actInd.startAnimating()
                break
            // Player item failed. See error.
            case .unknown:
                self.actInd.startAnimating()
                break
                // Player item is not yet ready.
            }
        }else if keyPath == "playbackBufferEmpty"{
            self.actInd.startAnimating()
        }else if keyPath == "playbackLikelyToKeepUp"{
            self.actInd.stopAnimating()
        }else if keyPath == "playbackBufferFull"{
            self.actInd.stopAnimating()
        }
    }
    
    func updateVideoPlayerState() {
        guard let currentTime = player?.currentTime() else { return }
        let currentTimeInSeconds = CMTimeGetSeconds(currentTime)
        progressSlider.value = Float(currentTimeInSeconds)
        if let currentItem = player?.currentItem {
            let duration = currentItem.duration
            if (CMTIME_IS_INVALID(duration)) {
                return;
            }
            let currentTime = currentItem.currentTime()
            progressSlider.value = Float(CMTimeGetSeconds(currentTime) / CMTimeGetSeconds(duration))
            
            // Update time remaining label
            let totalTimeInSeconds = CMTimeGetSeconds(duration)
            let remainingTimeInSeconds = totalTimeInSeconds - currentTimeInSeconds
            let timeformatter = NumberFormatter()
            guard let sec = timeformatter.string(from: NSNumber(value: currentTimeInSeconds)) else {
                return
            }
            if sec == "\(tempSec)" {
                self.player?.pause()
                tempSec += 50
                let objTab = AdvertisementVC(nibName: "AdvertisementVC", bundle: .main)
                objTab.blockAddClosed = { str in
                    self.player?.play()
                }
                self.present(objTab, animated: true, completion: nil)
                self.isDoResume = true
            }
            
            let mins = remainingTimeInSeconds / 60
            let secs = remainingTimeInSeconds.truncatingRemainder(dividingBy: 60)
            timeformatter.minimumIntegerDigits = 2
            timeformatter.minimumFractionDigits = 0
            timeformatter.roundingMode = .down
            guard let minsStr = timeformatter.string(from: NSNumber(value: mins)), let secsStr = timeformatter.string(from: NSNumber(value: secs)) else {
                return
            }
            timeRemainingLabel.text = "\(minsStr):\(secsStr)"
        }
    }
    
    @objc func hideControls() {
        playPauseButton.isHidden = true
        settings.isHidden = true
        backButton.isHidden = true
        progressSlider.isHidden = true
        timeRemainingLabel.isHidden = true
        forwardButton.isHidden = true
        rewindButton.isHidden = true
        titleLabel.isHidden = true
    }
    
    func resetTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(hideControls), userInfo: nil, repeats: false)
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
}
