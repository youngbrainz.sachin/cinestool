//
//  InstructionVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 03/09/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class InstructionVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var txtviewData: UITextView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    var str_instruction = ""
    var blockInstructionDone: (()->())!
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtviewData.attributedText = str_instruction.htmlAttributed(family: "Poppins-Medium", size: 16.0)
        self.btnBack.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 18.0)
    }
    
    //MARK:- Button tapped events
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.blockInstructionDone()
    }
}
