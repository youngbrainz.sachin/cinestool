//
//  PaymentPopupVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class PaymentPopupVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var txtViewTitle: UITextView!
    @IBOutlet weak var txtviewDesc: UITextView!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var txtviewTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var txtviewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtviewTop: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    var PopupFile: PopupType = .none
    var isFrom: screenFrom = .none
    var price = ""
    var currencySymbol = ""
    var movieID = ""
    var subscriptionPrice = ""
    var actual_price = ""
    var actual_symbol = ""

    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let countryInfo = getCountryCodeFromDialcode(dialCode: appDel.loggedInUserData?.country_code ?? "+1")
        let codeInfo = countryInfo["country"] as! String
        let currencyCodedetail = Locale.currencies[codeInfo]
        let currencyCode = getCurrencySymbol(forCurrencyCode: currencyCodedetail?.code ?? "USD")
        currencySymbol = currencyCode ?? "$"
        self.setUpControls()
    }
    
    func getSymbol(forCurrencyCode code: String) -> String? {
        let locale = NSLocale(localeIdentifier: code)
        if locale.displayName(forKey: .currencySymbol, value: code) == code {
            let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
            return newlocale.displayName(forKey: .currencySymbol, value: code)
        }
        return locale.displayName(forKey: .currencySymbol, value: code)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        txtviewDesc.setContentOffset(CGPoint.zero, animated: false)
        txtViewTitle.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func setUpControls() {
        txtViewTitle.scrollRangeToVisible(NSMakeRange(0, 0))
        txtviewDesc.scrollRangeToVisible(NSMakeRange(0, 0))
        txtviewHeight.constant = self.txtviewDesc.contentSize.height + 8
        
        let font:UIFont? = UIFont(name: "Poppins-SemiBold", size:70)
        let fontSuper:UIFont? = UIFont(name: "Poppins-SemiBold", size:40)
        if isFrom == .advertisement {
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: "\(currencySymbol)\(subscriptionPrice)", attributes: [.font:font!])
            attString.setAttributes([.font:fontSuper!,.baselineOffset:22], range: NSRange(location:0,length:currencySymbol.count))
            self.lblPrice.attributedText = attString
        } else {
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: "\(currencySymbol)\(price)", attributes: [.font:font!])
            attString.setAttributes([.font:fontSuper!,.baselineOffset:22], range: NSRange(location:0,length:currencySymbol.count))
            self.lblPrice.attributedText = attString
        }
      
        if PopupFile == .SkipAds {
            txtviewTitleHeight.constant = 0
            txtviewTop.constant -= 20
            txtviewDesc.textColor = AppColors.popupDarkFontColor
            viewHeight.constant = (self.viewHeight.constant + self.txtviewDesc.contentSize.height) - 90
        } else if PopupFile == .PaidNow {
            txtviewDesc.text = "This Movie will stay on your Cinestool Account until (120 Hours or five (5) days) to watch. Afterwards, the Movie will not be available on your account however, you can watch again."
            txtviewTitleHeight.constant = self.txtViewTitle.contentSize.height
            txtViewTitle.textColor = AppColors.popupDarkFontColor
            txtviewDesc.textColor = AppColors.popupLightFontColor
            viewHeight.constant = (self.viewHeight.constant + self.txtviewDesc.contentSize.height) - 50
        } else {
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnClosePopupTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPayNowTapped(_ sender: UIButton) {
        let objTab = PaymentVC(nibName: "PaymentVC", bundle: nil)
        objTab.isFrom = self.isFrom
        objTab.movieID = movieID
        objTab.price = self.price
        objTab.subscriptionPrice = subscriptionPrice
        objTab.actual_symbol = actual_symbol
        objTab.actual_price = self.actual_price
        self.present(objTab, animated: true, completion: nil)
    }
}
