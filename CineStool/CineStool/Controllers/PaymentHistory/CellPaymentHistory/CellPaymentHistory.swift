//
//  CellPaymentHistory.swift
//  CineStool
//
//  Created by Dharam YB on 18/02/20.
//  Copyright © 2020 Youngbrainz. All rights reserved.
//

import UIKit

class CellPaymentHistory: UITableViewCell, ReusableView, NibLoadableView {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    
    @IBOutlet weak var lblTransId: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
