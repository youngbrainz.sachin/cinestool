//
//  AdvertisementVC.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SDWebImage

class AdvertisementVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var imgAdvertise: UIImageView!
    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var lblAdTime: UILabel!
    
    var blockAddClosed: ((String)->())!
    var advertisementData: Advertisement!
    var movieID = ""
    var timer = Timer()
    var totalSecond = 90

    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        onloadOperation()
    }
    
    func onloadOperation() {
        apiCallGetAdvertisement()
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        self.lblAdTime.text = timeFormatted(totalSecond)
        if totalSecond != 0 {
            if totalSecond == 85 {
                btnSkip.isHidden = false
            }
            totalSecond -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        timer.invalidate()
        self.dismiss(animated: true) {
            self.blockAddClosed("0")
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        return String(format: "0:%02d", totalSeconds)
    }
    
    func apiCallGetAdvertisement() {
        
        WebService.Request.patch(url: getAdvertisement, type: .post, parameter: [:]) { (response, error) in
            if error == nil {
                if response!["status"] as? Int == 1 {
                    if let data = response!["data"] as? [String: Any] {
                        print(data)
                        self.advertisementData = Advertisement(JSON: data)
                        if self.advertisementData.advertisement_type == "1" {
                            self.startTimer()
                            self.imgAdvertise.sd_setImage(with: URL(string: self.advertisementData.advertisement_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "HomeLogo"))
                        } else {
                            self.startTimer()
                            self.playVideo(url: self.advertisementData.advertisement_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                            self.btnDismiss.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            self.btnDismiss.tintColorDidChange()
                        }
                    }
                } else {
                    self.alertOk(title: "", message: response!["msg"] as! String)
                }
            } else {
                self.alertOk(title: "", message: "Something went wrong please try again!")
            }
        }
    }
    
    func playVideo(url: String) {
        let videoURL = URL(string: url) //"https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
        let player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = imgAdvertise.bounds
        imgAdvertise.layer.addSublayer(playerLayer)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        player.play()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        if appDel.isAppLive == appStatus.Live{
            let objTab = PaymentPopupVC(nibName: "PaymentPopupVC", bundle: nil)
            objTab.isFrom = .advertisement
            objTab.PopupFile = .SkipAds
            objTab.price = advertisementData.advertisement_price
            objTab.subscriptionPrice = advertisementData.advertisement_price
            objTab.actual_price = advertisementData.advertisement_actual_price
            objTab.actual_symbol = advertisementData.currency_actual_symbol
            objTab.movieID = movieID
            objTab.modalPresentationStyle = .overFullScreen
            appDel.blockPaymentDone = {
                self.dismiss(animated: true, completion: {
                    self.blockAddClosed("1")
                })
            }
            self.present(objTab, animated: true, completion: nil)
        }else{
            self.dismiss(animated: true) {
                self.timer.invalidate()
                self.blockAddClosed("0")
            }
        }
    }
    
    @IBAction func btnSkipTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.timer.invalidate()
            self.blockAddClosed("0")
        }
    }
}

extension AdvertisementVC {
    @objc func playerDidFinishPlaying(note: NSNotification) {
        self.dismiss(animated: true) {
            self.blockAddClosed("0")
        }
    }
}
