//
//  CountryListVC.swift
//  SMA
//
//  Created by Jignesh Kugashiya on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class CountryListVC: UIViewController {

    //MARK:- Vriables & Outlets
    @IBOutlet weak var tblCountryList: UITableView!
    @IBOutlet weak var CountrysearchBar: UISearchBar!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var ConutrinData = NSMutableArray()
    var TempConutrinData = NSMutableArray()
    var selectedCountry : (([String:Any])->())?
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblCountryList.register(CellCountryList.self)

        let temp = Common.getCountryList() 
        self.TempConutrinData.addObjects(from:temp)
        self.ConutrinData = NSMutableArray()
        self.ConutrinData = self.TempConutrinData
        self.tblCountryList.reloadData()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension CountryListVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.ConutrinData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CellCountryList = tableView.dequeueReusableCellTb(for: indexPath)
        let data = ConutrinData[indexPath.row] as? [String:Any]
        cell.lblName.text = data?["name"] as? String ?? ""
        cell.lblCode.text = data?["code"] as? String ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        let data = ConutrinData[indexPath.row] as? [String:Any]
        //appDelegate.countryCode = //data?["code"] as? String ?? "0"
        selectedCountry?(data!)
        self.dismiss(animated: true, completion: nil)
    }
}

extension CountryListVC: UISearchBarDelegate, UIGestureRecognizerDelegate
{
    //MARK:- Search Bar Delegate Method
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        if (searchBar.text?.isEmpty)!
        {
            self.ConutrinData = self.TempConutrinData
            self.tblCountryList.reloadData()
        }
        self.view.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.returnKeyType = .done
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if (searchText.isEmpty)
        {
            self.ConutrinData = self.TempConutrinData
            self.tblCountryList.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let str = self.CountrysearchBar.text! + text
        if(text.isEmpty)
        {
            if (searchBar.text?.count == 1) && (str.count == 1)
            {
                self.ConutrinData = self.TempConutrinData
                self.tblCountryList.reloadData()
                return true
            }
            
            let tempStr = String(str.dropLast())
            self.filterArray(tempStr)
            return true
        }
        self.filterArray(str)
        return true
    }
    
    //MARK:- User Define Method
    
    func filterArray(_ str: String)
    {
        let strFinal = String(str.filter { !" \n\t\r".contains($0) })
        let predicate = NSPredicate(format: "name CONTAINS[c] %@", strFinal)
        let arrTemp = self.TempConutrinData.filtered(using: predicate)
        self.ConutrinData = NSMutableArray()
        self.ConutrinData.addObjects(from: arrTemp)
        self.tblCountryList.reloadData()
    }
}
