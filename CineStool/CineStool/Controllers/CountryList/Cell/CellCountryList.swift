//
//  CellCountryList.swift
//  SMA
//
//  Created by Jignesh Kugashiya on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

class CellCountryList: UITableViewCell, ReusableView, NibLoadableView
{
    //MARK:- Variables & Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
