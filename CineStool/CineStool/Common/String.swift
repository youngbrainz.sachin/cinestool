//
//  String.swift
//  SMA
//
//  Created by Jignesh Kugashiya on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

struct statusType {
    static let notificatioStatus = "1"
}

struct statusActiveInactive {
    static let Active = "1"
    static let InActive = "0"
}

struct appStatus {
    static let Live = "1"
    static let Test = "2"
}



struct StringText {
    static let strCustomer = "customer"
    static let strVendor = "vendor"
    static let insertMobileNumber = "insertMobileNumber"
    static let fillAllFields = "fillAllFields"
    static let insertFirstName = "insertFirstName"
    static let insertLastName = "insertLastName"
    static let insertEmail = "insertEmail"
    static let insertValidEmail = "insertValidEmail"
    static let insertAddress = "insertAddress"
    static let videoNotAvail = "Something went wrong please try again later."
}

struct UserdefaultsConstants {
    static let userData = "userData"
    static let mobileNumber = "mobileNumber"
    static let mobileCode = "mobileCode"
    static let homeData = "homeData"
    static let isJustLogin = "islogin"
    static let isPressedNoToSubscriptionPopup = "pressedNo"
}


//MARK:- Alert Messages
let InsertCardName = "InsertCardName"
let InsertCardNumber = "InsertCardNumber"
let InsertCardExpiryDate = "InsertCardExpiryDate"
let InsertCVV = "InsertCVV"


