//
//  Colors.swift
//  SMA
//
//  Created by Jignesh Kugashiya on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

struct AppColors {
    static let redColor: UIColor = UIColor (red: 253.0/255.0, green: 85.0/255.0, blue: 88.0/255.0, alpha: 1.0)
    static let greyColor: UIColor = UIColor (red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
    
    static let blackColor:UIColor = UIColor.black
    static let whiteColor:UIColor = UIColor.white
    
    static let cancelColor:UIColor = UIColor(red: 253.0/255, green: 85.0/255, blue: 88.0/255, alpha: 1.0)
    static let deliverColor:UIColor = UIColor(red: 71.0/255, green: 221.0/255, blue: 78.0/255, alpha: 1.0)
    
    static let redBackgColor:UIColor = UIColor(red: 255.0/255, green: 0, blue: 38.0/255, alpha: 1.0)
    
    static let popupLightFontColor:UIColor = UIColor(red: 6.0/255, green: 28.0/255, blue: 50.0/255, alpha: 0.5)
    static let popupDarkFontColor:UIColor = UIColor(red: 6.0/255, green: 28.0/255, blue: 50.0/255, alpha: 1.0)
}
