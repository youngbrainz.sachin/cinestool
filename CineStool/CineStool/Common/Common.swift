//
//  Common.swift
//  SMA
//
//  Created by MacBook Air 002 on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import LocalAuthentication

struct Common {

    static func shadowWithCorner(_ control : UIControl,cornerRadius:CGFloat)
    {
        control.layer.masksToBounds = true
        control.layer.cornerRadius = cornerRadius
        control.clipsToBounds = false
        control.layer.shadowColor = UIColor.gray.cgColor
        control.layer.shadowOpacity = 0.5
        control.layer.shadowOffset = CGSize.zero
        control.layer.shadowRadius = 5
    }
    
    static func shadowForVeiw(_ control : UIView, radius:CGFloat,opacity:Float,shadowRadius:CGFloat)
    {
        control.layer.masksToBounds = true
        control.layer.cornerRadius = radius
        control.clipsToBounds = false
        control.layer.shadowColor = UIColor.gray.cgColor
        control.layer.shadowOpacity = opacity
        control.layer.shadowOffset = CGSize.zero
        control.layer.shadowRadius = shadowRadius
    }
    
    static func textfieldShadow(_ control : UITextField)
    {
        control.layer.backgroundColor = UIColor.white.cgColor
        control.layer.masksToBounds = false
        control.layer.cornerRadius = 4.0
        control.layer.shadowRadius = 3.0
        control.layer.shadowColor = UIColor.lightGray.cgColor
        control.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        control.layer.shadowOpacity = 0.4
    }
    
    static func getCountryList() -> [[String:Any]]
    {
        do
        {
            if let file = Bundle.main.url(forResource: "countries", withExtension: "json")
            {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any]
                {
                    let data = object["countries"] as? [[String:Any]]
                    return data!
                }
                else if let object = json as? [Any]
                {
                    print(object)
                }
                else
                {
                    print("JSON is invalid")
                }
            }
            else
            {
                print("no file")
            }
        }
        catch
        {
            print(error.localizedDescription)
        }
        return []
    }
    
    private static let suffix = ["", "K", "M", "B", "T", "P", "E"]
    
    public static func formatNumber(_ number: Double) -> String{
        var index = 0
        var value = number
        while((value / 1000) >= 1){
            value = value / 1000
            index += 1
        }
        return String(format: "%.1f%@", value, suffix[index])
    }
}

func setContiner(VC aVC: String, nibClass: UIViewController.Type, parent: UIViewController.Type, container: UIView, newController : ((UIViewController)->())? = nil) {
     let aVC = nibClass.init(nibName: aVC, bundle: .main)
    
    newController?(aVC)
    
    for aView in container.subviews {
        aView.removeFromSuperview()
    }
    
    aVC.view.frame = container.bounds
    container.addSubview(aVC.view)
}

 func setContinerOther(VC aVC: String, storyboardName:String, parent : UIViewController, container : UIView, newController : ((UIViewController)->())? = nil) {
    let st = UIStoryboard.init(name: storyboardName, bundle: nil)
    let aVC = st.instantiateViewController(withIdentifier: aVC)
    
    newController?(aVC)
    
    for aView in container.subviews{
        aView.removeFromSuperview()
    }
    
    for aChildVC in parent.children{
        aChildVC.removeFromParent()
    }
    
    aVC.view.frame = container.bounds
    container.addSubview(aVC.view)
    parent.addChild(aVC)
}

func findtopViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
    if let navigationController = controller as? UINavigationController {
        return findtopViewController(controller: navigationController.visibleViewController)
    }
    if let tabController = controller as? UITabBarController {
        if let selected = tabController.selectedViewController {
            return findtopViewController(controller: selected)
        }
    }
    if let presented = controller?.presentedViewController {
        return findtopViewController(controller: presented)
    }
    return controller
}

func resetDefaults(fromForgotNumber: Bool = false) {
    let biometricStatus = UserDefaults.standard.bool(forKey: "isTouchID")

    let defaults = UserDefaults.standard
    let dictionary = defaults.dictionaryRepresentation()
    dictionary.keys.forEach { key in
        defaults.removeObject(forKey: key)
    }
    if !fromForgotNumber{
        UserDefaults.standard.set(biometricStatus, forKey: "isTouchID")
        UserDefaults.standard.set(appDel.loggedInUserData?.phone_number ?? "", forKey: UserdefaultsConstants.mobileNumber)
        UserDefaults.standard.set(appDel.loggedInUserData?.country_code ?? "", forKey: UserdefaultsConstants.mobileCode)
        UserDefaults.standard.synchronize()
    }
}

func alertController(message: String , controller: UIViewController)
{
    let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
    
    let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        controller.dismiss(animated: true, completion: nil)
    }
    
    alertController.addAction(action1)
    controller.present(alertController, animated: true, completion: nil)
}

func getCountryCodeFromDialcode(dialCode: String) -> [String: Any] {
    let countryitem = Common.getCountryList()
    var country = [String: Any]()
    for item in countryitem{
        if item["code"] as? String == dialCode{
            country = item
        }
    }
    return country
}

func getCurrencySymbol(forCurrencyCode code: String) -> String? {
    let locale = NSLocale(localeIdentifier: code)
    if locale.displayName(forKey: .currencySymbol, value: code) == code {
        let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
        return newlocale.displayName(forKey: .currencySymbol, value: code)
    }
    return locale.displayName(forKey: .currencySymbol, value: code)
}

func isValidEmail(testStr:String) -> Bool
{
    let emailTest = NSPredicate(format:"SELF MATCHES %@", Email_RegEx)
    return emailTest.evaluate(with: testStr)
}


func addBlurBackground(control: UIImageView)  {
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.frame = control.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    control.addSubview(blurEffectView)
}

func addCornerOnTopLeftTopRight(control: UIView)  {
    let rectShape1 = CAShapeLayer()
    rectShape1.bounds = control.frame
    rectShape1.position = control.center
    let frame = CGRect (x: 0, y: control.bounds.origin.y, width: UIScreen.main.bounds.size.width, height: control.bounds.size.height)
    rectShape1.path = UIBezierPath(roundedRect: frame, byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 30, height: 30)).cgPath
    control.layer.mask = rectShape1
}

func addCornerOnBottomLeftTopRight(control: UIView)  {
    let rectShape1 = CAShapeLayer()
    rectShape1.bounds = control.frame
    rectShape1.position = control.center
    let frame = CGRect (x: 0, y: control.bounds.origin.y, width: UIScreen.main.bounds.size.width, height: control.bounds.size.height)
    rectShape1.path = UIBezierPath(roundedRect: frame, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 30, height: 30)).cgPath
    control.layer.mask = rectShape1
}

//MARK:- Touch ID
func authenticationWithTouchID(completion: (()->())!) {
    let localAuthenticationContext = LAContext()
    localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
    var authError: NSError?
    let reasonString = "Authenticate with Touch ID"
    if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
        localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reasonString) { success, evaluateError in
            if success {
                completion()
            } else {
                guard let error = evaluateError else {
                    return
                }
                print(evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
            }
        }
    } else {
        guard let error = authError else {
            return
        }
        print(evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
    }
}

func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
    var message = ""
    if #available(iOS 11.0, macOS 10.13, *) {
        switch errorCode {
        case LAError.biometryNotAvailable.rawValue:
            message = "Authentication could not start because the device does not support biometric authentication."
            
        case LAError.biometryLockout.rawValue:
            message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
            
        case LAError.biometryNotEnrolled.rawValue:
            message = "Authentication could not start because the user has not enrolled in biometric authentication."
            
        default:
            message = "Did not find error code on LAError object"
        }
    } else {
        switch errorCode {
        case LAError.touchIDLockout.rawValue:
            message = "Too many failed attempts."
            
        case LAError.touchIDNotAvailable.rawValue:
            message = "TouchID is not available on the device"
            
        case LAError.touchIDNotEnrolled.rawValue:
            message = "TouchID is not enrolled on the device"
            
        default:
            message = "Did not find error code on LAError object"
        }
    }
    return message;
}
func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
    
    var message = ""
    
    switch errorCode {
        
    case LAError.authenticationFailed.rawValue:
        message = "The user failed to provide valid credentials"
        
    case LAError.appCancel.rawValue:
        message = "Authentication was cancelled by application"
        
    case LAError.invalidContext.rawValue:
        message = "The context is invalid"
        
    case LAError.notInteractive.rawValue:
        message = "Not interactive"
        
    case LAError.passcodeNotSet.rawValue:
        message = "Passcode is not set on the device"
        
    case LAError.systemCancel.rawValue:
        message = "Authentication was cancelled by the system"
//        authenticationWithTouchID(completion: nil)
    case LAError.userCancel.rawValue:
        message = "The user did cancel"
//        authenticationWithTouchID(completion: nil)
    case LAError.userFallback.rawValue:
        message = "The user chose to use the fallback"
    default:
        message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
    }
    return message
}
