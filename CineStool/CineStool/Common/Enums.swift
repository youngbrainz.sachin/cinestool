//
//  Enums.swift
//  CineStool
//
//  Created by Jignesh Kugashiya on 11/07/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import Foundation


enum CommonFileType
{
    case PrivacyPolicy
    case TermsAndCondition
    case none
}

enum PopupType
{
    case SkipAds
    case PaidNow
    case none
}

enum screenFrom {
    case mobileVerify
    case home
    case advertisement
    case movieDetail
    case settingPayment
    case none
}

enum redirectToMoviePlayerFrom{
    case Movie
    case Trailer
    case Banner
}

enum verifyOTPFrom {
    case mobileScreen
    case emailScreen
    case changeMobileScreen
    case changePasswordScreen
    case none
}
